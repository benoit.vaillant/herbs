#!/usr/bin/perl -w

###############################################################################
# mkattributes.pl : programme pour passer de donn�es brutes � des donn�es     #
#    d�crites par des noms d'attributs.                                       #
#                                                                             #
# ex.: 1,1,1,0        att1=1,att2=1,att3=1,att4=0                             #
#      0,1,0,0   ==>  att1=0,att2=1,att3=0,att4=0                             #
#      1,1,0,1        att1=1,att2=1,att3=0,att4=1                             #
#                                                                             #
# Copyright (C) 2003 : Beno�t Vaillant, benoit.vaillant@enst-bretagne.fr      #
#                                                                             #
# This program is free software; you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation; either version 2 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Library General Public License for more details.                        #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program; if not, write to the Free Software                 #
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  #
###############################################################################


use strict;
use Term::ReadKey;

#############
# Variables #
#############

my ($i, $j, $k, $data, $names, $out, $type, @noms, @lignes, %typs, @cas, @champs, @fin);

if ($#ARGV != 2) { die "Utilisation de multi2uni.pl : multi2uni.pl data names outfile\n"; }

$data=$ARGV[0];
$names=$ARGV[1];
$out=$ARGV[2];

if (! -f $data) {
    die "$data n'est pas un fichier\n";
  }
if (! -f $names) {
    die "$names n'est pas un fichier\n";
  }

open(DATA, "<$data") || die "impossible de lire le fichier $data\n";
open(NAMES, "<$names") || die "impossible de lire le fichier $names\n";
open(SORTIE, ">$out") || die "impossible d'�crire dans le fichier $out\n";

while(<NAMES>) {
  ($i = $_) =~ s/[:\|#\ ].*\n|\n//g; # on vire les commentaires
  if (! $i =~ /^$/) {
    push @noms, $i;
    ($j = $_) =~ s/$i[:\ ]*//;
    $j =~ s/\.*\n//;
    push @lignes, $j;
  }
}

undef(%typs);
ReadMode('cbreak');

foreach $i ( reverse @noms ) {
  $j = pop @lignes;
  $type = '';
  while (! ($type =~ /[v|e||i|r]/)) {
    print "attribut : $i ($j)\n";
    print "Type : [v]aleur (entier, r�el)\n";
    print "       [e]um�r� (ou chaine de caract�re)\n";
    print "       [i]ntervalle (du type att<=val...)\n";
    print " [R]enommer\n";
    print "Choix : ";
    $type = ReadKey(0);
    if (! ($type =~ /[v|e|i|R]/)) {
      print "\nchoix incorrect !\n\n";
    }
    if ($type =~ /[R]/) {
      print "\nNouveau nom : ";
      $i="";
      while(1) {
	$k = ReadKey(0);
	if ($k =~ /\n/) {
	  print "\n";
	  last;
	}
	elsif ($k =~ /[A-z]/) {
	$i .= $k;
	print $k;
      }
      }
    }
  }
  print "$type\n\n";
  $typs{$i} = $type;
  push @champs, $i;
}

ReadMode('normal');

@champs = reverse @champs;

print "g�n�ration de la base de cas...\n";

$j = select(SORTIE);

while (<DATA>) {
  @cas = split(/[ |,|\n]/, $_);
  undef @fin;
  $i = 0;
  foreach $k (@cas) {
    if ($typs{$champs[$i]} =~ /[v]/) {
      push @fin, $champs[$i]."=".$k;
    }
    elsif ($typs{$champs[$i]} =~ /[e]/) {
      push @fin, $champs[$i]."='".$k."'";
    }
    else { # intervalle, auto-d�fini
      push @fin, $champs[$i].$k;
    }
    $i++;
  }
  print join(" ", @fin);
  print "\n";

}

select($j);

close(DATA);
close(NAMES);
close(SORTIE);
