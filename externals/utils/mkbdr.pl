#!/usr/bin/perl -w

###############################################################################
# mkbdr.pl : programme pour passer de donn�es format�es par mkattributes.pl   #
#    � des bases des r�gles, utilisant l'algorithme apriori. Pour chaque      #
#    base de regles, ce script retourne deux fichiers, l'un etant la sortie   #
#    de l'algorithme apriori (extension .out), l'autre �tant la conversion au #
#    format SQL de cette base de regles.                                      #
#                                                                             #
# Copyright (C) 2003 : Beno�t Vaillant, benoit.vaillant@enst-bretagne.fr      #
#                                                                             #
# This program is free software; you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation; either version 2 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Library General Public License for more details.                        #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program; if not, write to the Free Software                 #
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  #
###############################################################################

use strict;

#########################
# Bases de cas pr�tes : #
#########################

my ($sup, $conf);
my @bdc = ("abalone", 2, 50,
#	   "adult", 10, 70,
	   "autompg", 5, 50,
	   "balancescale", 5, 50,
	   "breastcancerwisconsin", 10, 70,
	   "car", 5, 60,
	   "cmc", 5, 60,
#	   "covtype", 10, 70,
	   "cpuperformance", 2, 60,
	   "ecoli", 2, 60,
	   "flags", 50, 90,
	   "housing", 2, 55,
	   "letterrecognition", 2, 55,
#	   "spambase", 10, 70,
	   "solarflare1", 20, 85,
	   "solarflare2", 20, 85);

#############
# Variables #
#############

my ($i, $j, $k, $in, $out, $app, @args);

############
# mkbdr.pl #
############

while ( $#bdc > 1) {
    $conf = pop @bdc;
    $sup = pop @bdc;
    $i = pop @bdc;

    print "G�n�ration de la base de r�gles pour la base de cas ".$i."\n";
    
    $in = "BDC-ok/".$i.".in";
    $out = "BDR/".$i.".out";
    $app = "BDC-ok/".$i.".app";

    if (!-r $app) { $app=""; }
    if (! -f $in) { next; }

    print "[ex�cution de :] apriori -m2 -f\" ,\\t\" -s".$sup." -c".$conf." ".$in." ".$out." ".$app."\n";
    @args = ("apriori -m2 -f\" ,\\t\" -s$sup -c$conf $in $out $app");
    system(@args);
    print "conversion au format SQL";
    open(DATA, "<$out") || die "impossible de lire le fichier $out\n";
    $out =~ s/\.out//;
    open(SQL, ">$out.sql") || die "impossible d'�crire dans le fichier $out.sql\n";
    $out =~ s/.*\///g;
    select(SQL);
    print "use HERBS\n";
    print "drop table if exists bdr_".$out."_s".$sup."c".$conf."_apriori;\n";
    print "create table bdr_".$out."_s".$sup."c".$conf."_apriori (id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, premisse text, conclusion text);\n";
     while(<DATA>) {
 	($j = $_) =~ s/\ +\([0-9\.%,\ ]*\)\n//g; # on vire la fin des lignes
 	$j =~ s/\ <-\ /\",\"/g;
 	$j =~ s/\ / and /g;
 	print "insert into bdr_".$out."_s".$sup."c".$conf."_apriori (conclusion,premisse) values (\"".$j."\");\n";
     }
     close(DATA);
     close(SQL);
     select(STDOUT);
}
