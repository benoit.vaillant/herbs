#! /usr/bin/perl -w

###############################################################################
# parsedt.pl : ce programme convertit un arbre de d�cision issu de C4.5 en    #
#    r�gles de la forme premisse -> conclusion sous forme de requ�tes SQL     #
#    afin de les int�grer dans une base de donn�es.                           #
#                                                                             #
# Copyright (C) 2003 : Beno�t Vaillant, benoit.vaillant@enst-bretagne.fr      #
#                                                                             #
# This program is free software; you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation; either version 2 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Library General Public License for more details.                        #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program; if not, write to the Free Software                 #
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  #
###############################################################################

# D�finition des chaines de caract�res � utiliser pour la sortie des donn�es

$link = " and "; # machin entre les diff�rents attributs d'une r�gle
$impl = "', '"; # machin entre la pr�misse et la conclusion
$prec = "insert into maTable (premisse, conclusion) values ('"; # machin au d�but de la r�gle
$end = "');"; # machin � la fin de la r�gle

sub purgeAndCount {
  my ($string, $substring) = @_;
  my @tmp;
  @tmp = split(/\|\ \ \ /, $string);
  $string = $tmp[ $#tmp ];
  ($string) = split(/:/, $string);
  return ($#tmp, $string);
}

sub mkRules {
  my ($treeref, $treesref) = @_;
  my ($i, $count, $str, $line, @result, @STrules, @tmp);
  my @strlist = ();
  foreach $line ( @$treeref ) {
    ($count, $str) = purgeAndCount($line);
    if (($count == $#strlist) || ($#strlist == -1)) { # remplacer le dernier �l�ment de $strlist si il y en a un, le mettre dans la liste sinon
      if ($#strlist == -1) #liste vide
	{
	  @strlist = ($str);
	}
      else
	{
	  $strlist[$#strlist] = $str;
	}
    }
    elsif ($count == $#strlist + 1) { # ajouter $str � la suite de la liste
      push @strlist, $str;
    }
    else { # r�duire la liste
      splice(@strlist, $count);
      push @strlist, $str;
    }
    if ($line =~ /\]$/) { # sous-arbre
      @tmp = split(/:/, $line);
      $str = $tmp[$#tmp];
      $str =~ s/[Subtre \[\]]//g;
      if ($str =~ /\?\?/) { # hum, c4.5 se chie dessus...
	print "C4.5 c'est chi� dessus dans les sous-arbres. J'peux pas bosser dans ces conditions !\n";
	close INPUT;
	exit;
      }
      else {
	$i = $str; # cast implicite
	@tmp = @{ @$treesref[$i] };
	@STrules = mkRules(\@tmp, $treesref);
	foreach $str ( @STrules )
	  {
	    $tmp = join($link, @strlist).$impl.$str;
	    $tmp =~ s/(\ \ )/\ /g; # hack pour que �a fasse joli
	    push @result, $tmp;
	  }
      }
    }
    elsif ($line =~ /\)$/) { # c'est une feuille de l'arbre, faut pusher la chose et foutre la classe
      @tmp = split(/:/, $line); # faut r�cup�rer la classe
      @tmp = split(/\(/, $tmp[$#tmp]);
      $tmp = @tmp[0];
      $tmp = join($link, @strlist).$impl.$tmp;
      $tmp =~ s/(\ \ )/\ /g; # hack pour que �a fasse joli
      push @result, $tmp;
    }
  }

  return @result;
}

if ($#ARGV != 0) { die "Utilisation de parsedt : parsedt dtfile\n"; }

$dtfile=$ARGV[0];
if (! -r $dtfile) {
    die "impossible de lire le fichier $dtfile\n";
    }

if (! -f $dtfile) {
    die "$dtfile n'est pas un fichier\n";
}

open(INPUT, "<$dtfile") || die "impossible de lire le fichier $dtfile\n";

$i = -1;

#@tmp;
#@trees;

while (<INPUT>) { # on lit toutes les lignes
    if ($i == -1) { # on cherche le d�but de l'arbre de d�cision
	if ($_ =~ /^Decision Tree:$/) { $i++;} # trouv�
    }
    else { # ben on peut y aller
	if (($_ =~ /^Tree saved$/) || ($_ =~ /^Simplified Decision Tree:$/)) { # termin� de parser : g�n�rer les r�gles
	  @{ $trees[$i] } =  @tmp; # on ajoute � la liste d'arbres celui qui vient d'�tre lu
	  #mkRules(\@{ $trees[0] } , \@trees);

	  @rules = mkRules(\@{ $trees[0] } , \@trees);
	  foreach $line ( @rules ) {
	    $line =~ s/(\ $)//g; # hack pour que �a fasse joli
	    print $prec."$line".$end."\n";
	  }
	  close INPUT;
	  exit; # fini :)
	}
	elsif (!($_ =~ /^$/)) { # ligne non vide
	    if ($_  =~ /^Subtree \[S[0-9]*\]$/) { # faut passer � une nouvelle liste
	      @{ $trees[$i] } =  @tmp; # sauvegarde du boulot d�j� fait
	      @tmp = (); # on efface
	      $str = $_;
	      $str =~ s/[Subtre \[\]]//g;
	      $i = $str; # cast implicite
	    }
	    else {
		push @tmp, ($_);
	    }
	}
    }
}

# si on arrive l�, c'est que le fichier est pas complet

close INPUT;
die "Fichier non complet !\n";
