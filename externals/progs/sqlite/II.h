#ifndef _INTENSITE_IMPLICATION_H_
#define _INTENSITE_IMPLICATION_H_

/** Cumulative distribution function of the standard normal distribution */
double phi(double x);

/** Intensity of implication */
double II(long int n, long int na, long int nb, long int nab);

/** Entropic intensity of implication */
double EII(long int n, long int na, long int nb, long int nab);

/**  */
double IPD(double x, double mean, double std_dev);

#endif
