#include "calculIntensite.h"

/* _________________________________________________________________________ */

int
main (int argc, char * argv[])
{
  t_regle R1 = {58,11,577,1473}; /* na nab nb n */
  t_regle R2 = {70,38,90,500};
  t_regle R3 = {90,38,90,500};
  t_regle R4 = {70,38,90,5000};

  double iic, iie;

  printf ("iic(R1), iie(R1) = (%.4f, %f)\n", IIC (R1), IIE(R1));
  printf ("iic(R2), iie(R2) = (%.4f, %f)\n", IIC (R2), IIE(R2));
  printf ("iic(R3), iie(R3) = (%.4f, %f)\n", IIC (R3), IIE(R3));
  printf ("iic(R4), iie(R4) = (%.4f, %f)\n", IIC (R4), IIE(R4));

  return(0);
}
