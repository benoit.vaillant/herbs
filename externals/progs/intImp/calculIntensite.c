#include "calculIntensite.h"
#include "loi_normale.h"


/* -------------------------- FONCTIONS PRIVEES  ----------------------- */

static double log2 (double x) { return (log(x) / log(2.0)); }

/* ---- */

static double indiceImplication (t_regle r)
{
  double pab = (double) r.Nab / (double) r.N;
  double pa = (double) r.Na / (double) r.N;
  double pb = (double) r.Nb / (double) r.N;

  double p_b = 1 - pb;
  double pa_b = pa - pab;

  if ((pa * p_b) == 0.0) { return 100.0; }
  else { return (sqrt ((double) r.N) * ( (pa_b - pa*p_b) / sqrt(pa * p_b) ));}
  
} /* End of indiceImplication */

/* ---- */

static double phi (double qa_b)
{
  float k, intensite;
  int i, j;
  
  k = 1000.0 * fabs(qa_b);
  /*  calcul des indices de la table de la loi normale centr�e r�duite */

  i = (int) k / 100.0;
  k = k - (double)i*100.0;
  j = (int)k / 10;
  /* 
     pour |Q| >= 6.0, nous consid�rons que la loi normale centr�e r�duite est �gale � 1
  */

  if (i >= 60) {intensite = 1.0;}
  else {intensite = DENSITE_LOI_NORMALE_CENTREE_REDUITE[i][j];}
  
  /* 
     la table de loi normale est fournie pour des valeurs de Q > 0, la fonction
     F de r�partition de la variable normale centr�e r�duite est telle que pour u>0, F(-u) = 1 - F(u)
  */

  if (qa_b < 0.0) {intensite = 1.0 - intensite;}

  return (1.0 - intensite);
} /* End of phi */

/* ---- */

static double coeff_entropique (t_regle r)
{
  double h1, h2, iab, produit_h1_h2;
  double pa, pb_, pab_;
  double N = (double) r.N;
  double Na = (double) r.Na;
  double Nab = (double) r.Nab;
  double Nb = (double) r.Nb;
  
  pa = Na /  N;
  pb_ = 1.0 - (Nb /  N);
  pab_ = pa - (Nab /  N);

  if ((0 <= pab_) && (pab_ < pa/2.0))
    { h1 = - (1- pab_ / pa) * log2 (1- pab_ / pa) - (pab_ / pa) * log2 (pab_ / pa); }
  else { h1 = 1.0; }
  
  if ((0 <= pab_) && (pab_ < pb_/2.0))
    { h2 = - (1- pab_ / pb_) * log2 (1- pab_ / pb_) - (pab_ / pb_) * log2 (pab_ / pb_); }
  else { h2 = 1.0; }
  
  produit_h1_h2 = (1.0 - pow(h1, 2)) * (1.0 - pow(h2, 2));
  iab = pow (produit_h1_h2, 0.25);

  return (iab);
} /* End of coeff_entropique */

/* -------------------------- FONCTIONS PUBLIQUES  ----------------------- */


double IIC (t_regle r) /* Intensite d'Implication Classique */
{
  return phi (indiceImplication (r));
} /* End of IIC */

double IIE (t_regle r) /* Intensite d'Implication Entropique */
{
  printf("coef ent : %f\n",coeff_entropique (r));
  return (sqrt (IIC (r) * coeff_entropique (r)));
} /* End of  IIE*/

