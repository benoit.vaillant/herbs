#ifndef _CALCUL_INTENSITE_H_
#define _CALCUL_INTENSITE_H_

#include <math.h>

typedef struct
{ 
  /* regle du type A->B */
  long Na, Nab, Nb; /* valeurs particulieres pour la regle */
  long N; /* nombre d'enregistrements dans la base */
} t_regle;

double IIC (t_regle);

double IIE (t_regle);

#endif
