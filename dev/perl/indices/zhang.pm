#! /usr/bin/perl

##########################################
# Copyright (C) 2002 Benoit Vaillant     #
# (benoit.vaillant@enst-bretagne.fr)     #
# This file is part of the HERBS project #
#                                        #
# see COPYRIGHT.TXT                      #
##########################################

package zhang;

sub new {
  my $classe = shift;
  my $self = { };
  bless($self, $classe);
  return $self;
}

sub nom {
  return "Zhang";
}

sub valeur {
    shift;
    my ($idCR, $idRegle, $n, $DB_NAME, $DB_HOST, $DB_PORT, $DB_USER_NAME, $DB_PASSWD) = @_;
    my $sth;
    my $bd = DBI->connect("DBI:mysql:$DB_NAME:$DB_HOST:$DB_PORT", $DB_USER_NAME, $DB_PASSWD, {PrintError => 0, RaiseError => 0 } ) or $erreur = 1;
    if ($erreur) {
	print("--\nImpossible de se connecter � la base de donn�e.\nV�rifiez les param�tres de configuration de HERBS.\n");
	return undef;
    }
    else {
	my $tmp;
	$sth = $bd->prepare("SELECT na, nb, nab from evaluations where idCR=$idCR and idRegle=$idRegle");
	$sth->execute;
	while(@row = $sth->fetchrow_array) { # un seul tuple normalement
	    $na = $row[0];
	    $nb = $row[1];
	    $nab = $row[2];

	    $tmp = $nb*($na-$nab);
	    if ($nab*($n-$nb) > $tmp) {
		$tmp = $nab*($n-$nb);
	    }
	    if (! $tmp) {
		return -3.402823466E38;
	    }
	    
	    $tmp = ($n*$nab-$na*$nb)/$tmp;
	    if ($tmp > 3.402823466E38) { $tmp = 3.402823466E38; }
	    if ($tmp < -3.402823466E38) { $tmp = -3.402823466E38; }
	    return $tmp;
	}
    }
}

sub hi {
    return 0;
}

return 1;
