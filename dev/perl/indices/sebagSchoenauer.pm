#! /usr/bin/perl

##########################################
# Copyright (C) 2002 Benoit Vaillant     #
# (benoit.vaillant@enst-bretagne.fr)     #
# This file is part of the HERBS project #
#                                        #
# see COPYRIGHT.TXT                      #
##########################################

package sebagSchoenauer;

sub new {
  my $classe = shift;
  my $self = { };
  bless($self, $classe);
  return $self;
}

sub nom {
  return "Sebag et Schoenauer";
}

sub valeur {
  shift;
  my ($idCR, $idRegle, $n, $DB_NAME, $DB_HOST, $DB_PORT, $DB_USER_NAME, $DB_PASSWD) = @_;
  my $sth;
  my $bd = DBI->connect("DBI:mysql:$DB_NAME:$DB_HOST:$DB_PORT", $DB_USER_NAME, $DB_PASSWD, {PrintError => 0, RaiseError => 0 } ) or $erreur = 1;
  if ($erreur) {
    print("--\nImpossible de se connecter � la base de donn�e.\nV�rifiez les param�tres de configuration de HERBS.\n");
    return undef;
  }
  else {
    my $tmp;
    $sth = $bd->prepare("SELECT na, nab_ from evaluations where idCR=$idCR and idRegle=$idRegle");
    $sth->execute;
    while(@row = $sth->fetchrow_array) { # un seul tuple normalement
      $na = $row[0];
      $nab_= $row[1];
      if (!$nab_) {
	$tmp = 3.402823466E38;
      }
      else {
	$tmp = $na/$nab_-1;
	if ($tmp > 3.402823466E38) { $tmp = 3.402823466E38; }
      }
      return $tmp;
    }
  }
}

sub hi {
  shift;
  my ($idCR, $idRegle, $n, $DB_NAME, $DB_HOST, $DB_PORT, $DB_USER_NAME, $DB_PASSWD) = @_;
  my $sth;
  my $bd = DBI->connect("DBI:mysql:$DB_NAME:$DB_HOST:$DB_PORT", $DB_USER_NAME, $DB_PASSWD, {PrintError => 0, RaiseError => 0 } ) or $erreur = 1;
  if ($erreur) {
    erreur_dialog("Impossible de se connecter � la base de donn�e.\nV�rifiez les param�tres de configuration de HERBS.");
    return undef;
  }
  else {
    $sth = $bd->prepare("SELECT nb from evaluations where idCR=$idCR and idRegle=$idRegle");
    $sth->execute;
    while(@row = $sth->fetchrow_array) { # un seul tuple normalement
      $nb = $row[0];
      if ($nb == $n) { return 3.402823466E38; }
      return $nb/($n-$nb);
    }
  }
}

return 1;
