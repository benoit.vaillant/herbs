python ./herbscmd.py -c test.db 
python ./herbscmd.py test.db rulesset import -a -n 'car_apriori' app/tests/data/rules.apriori
#ERR python ./herbscmd.py test.db import --c45 -n 'car_c45' app/tests/data/car.c45
python ./herbscmd.py test.db rulesset import --c45 -t offer -n 'car_c45' app/tests/data/car.c45 
python ./herbscmd.py test.db dataset import --csv app/tests/data/abalone_with_headers.csv
#ERR unhandled
# python ./herbscmd.py test.db import --csv buying:maint:doors:persons:lug_boot:safety:offer app/tests/data/car.data 
python ./herbscmd.py test.db dataset import --csv -l buying:maint:doors:persons:lug_boot:safety:offer app/tests/data/car.data 

python ./herbscmd.py test.db list --all --evaluations

python ./herbscmd.py ./test.db comp -q SUP:car_apriori:car
python ./herbscmd.py ./test.db comp -q PS:car_apriori:car
python ./herbscmd.py ./test.db comp -e car_apriori:car
python ./herbscmd.py ./test.db comp -E -Q


time python ./herbscmd.py test.db dataset import --csv -l Att1:Att2:Att3:Att4:Att5:Att6:Att7:Att8:Att9:Att10:Att11:Att12:Att13:Att14:Att15:Att16:Att17:Att18:Att19:Att20:Class ../../data/german.apriori.data.csv 
#real    0m5.421s     pvaillant: real    0m0.535s
#user    0m4.307s	             user    0m0.498s
#sys     0m0.087s	             sys     0m0.013s

# PRAGMA synchronous OFF;
#real    0m0.667s
#user    0m0.599s
#sys     0m0.010s

time python ./herbscmd.py test.db rulesset import -a -n 'german_apriori_small' ../../data/german.apriori.small.rules 

# real    0m4.770s                real    0m6.634s
# user    0m4.063s				  user    0m4.042s
# sys     0m0.057s				  sys     0m2.168s

# real    0m7.357s                real    0m6.571s
# user    0m5.612s				  user    0m3.998s
# sys     0m1.522s				  sys     0m2.309s

## BZ2
time python ./herbscmd.py test.db rulesset import -a -n 'german_apriori_huge' ../../data/german.apriori.huge.rules.bz2 

#real    27m13.528s
#user    24m6.350s
#sys     0m25.818s

##  time bunzip2 -k german.apriori.huge.rules.bz2 
## 
## real    1m7.353s
## user    0m58.851s
## sys     0m2.465s

## Regular file
time python ./herbscmd.py test.db rulesset import -a -n 'german_apriori_huge' ../../data/german.apriori.huge.rules 
#HERBS 2004-08-02 10:45:27,576 rules_set.py:323 DEBUG reading line 500000 after 67.539s
#HERBS 2004-08-02 11:01:58,718 rules_set.py:323 DEBUG reading line 6540000 after 1058.682s
#
#real    17m40.370s        real    21m37.483s
#user    14m22.050s		   user    13m54.584s
#sys     0m23.535s		   sys     7m29.734s 

#pragma synchronous=off
# real    28m5.380s        real    23m40.510s
# user    19m28.635s	   user    15m13.132s
# sys     6m11.631s		   sys     7m48.301s 





time python ./herbscmd.py test.db comp -e german_apriori_small:german_apriori_data


time python ./herbscmd.py test.db comp -e german_apriori_huge:german_apriori_data
