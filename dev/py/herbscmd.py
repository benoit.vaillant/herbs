#! /usr/bin/env python
# -*- coding: iso-8859-1 -*-
## LICENSE
"""
A command-line utility for herbs
"""
import getopt, logging, sys

if __name__ == '__main__' and '--local' in sys.argv:
  import dev_utils
  dev_utils.fixpath()
  sys.argv.remove('--local')

from herbs.utils import log

from herbs.app import cmd_init, cmd_dataset, cmd_rulesset, cmd_distribution
from herbs.app import cmd_compatibility, cmd_list

class cmd_show:
  def parse(self, args):
    if len(args)>1:
      raise ValueError, "command 'show' should be the last one"
    return args[1:]
  def run(self):
    from matplotlib.pylab import show
    show()
  def usage(self):
    sys.stderr.write("show the figures built with command distribution\n")
    
_available_commands={ 'init': cmd_init,
                     'use': cmd_init,
                      ('compatibility','comp'): cmd_compatibility,
                      'list': cmd_list,
                      ('dataset','ds'): cmd_dataset,
                      ('rulesset','ruleset','rs'): cmd_rulesset,
                      ('distribution','distrib','dist'): cmd_distribution,
                      ('show'): cmd_show, # should be the last one
                     }
available_commands={}
for n,v in _available_commands.items():
  if type(n) is not type(()):
    n=(n,)
  for c in n:
    available_commands[c]=v

del _available_commands
#check_commands(commands)

def usage(prg_name):
  """Usage: %(prg_name)s [options] db_name [command command-options]*
Options
-------
  -c                  create the database if it does not exist

  -v --verbose        verbose. Increases verbosity if set more than once
  -q --quiet          quiet
  -Q                  Really quiet

  -H <command>        Print the help for the supplied command
  --help-command <command>

  -h --help          this help

Available commands (and their aliases):
  init - use - dataset (ds) - rulesset (rs) - compatibility (comp) - list -
  distribution (distrib, dist) - show
"""  
  print usage.__doc__%{'prg_name':prg_name}
  

def main(args):
  me=args[0]
  if len(args)<2:
    usage(me)
    return 1

  create=False
  db_name=None
  use_init=False
  help_cmd=None
  show_matplotlib = False
  verbose=0
  try:
    options, args = getopt.getopt(args[1:],
                                  'hH:cQqv',
                                  ["help", "help-command=",
                                   "create", "quiet", "verbose"])
  except:
    usage(me); return 1
  for k, v in options:
    if k in ('-h', '--help'): usage(me); return 0
    if k in ('-H', '--help-command'): help_cmd=v; break
    if k in ('-c', '--create'): create=True; continue
    if k in ('-q', '--quiet'): log.setLevel(logging.WARNING); continue
    if k in ('-Q', ): log.setLevel(logging.ERROR); continue
    if k in ('-v', '--verbose'):
      verbose+=1
      if verbose==1:
        log.setLevel(logging.DEBUG)
      elif verbose==2:
        log.setLevel(logging.TRACE)
      # else: use default logging level: INFO (see utils)
        
  if help_cmd:
    cmd=available_commands.get(help_cmd)
    if cmd is None:
      sys.stderr.write("Unrecognized command: %s\n"%help_cmd)
      usage(me)
      return
    else:
      sys.stderr.write(cmd.usage())
      return

  if len(args)<1: usage(me) ; return 1

  if create:
    args.insert(0, 'init')
  else:
    args.insert(0, 'use')

  # Delegate the parsing of the other options to commands
  commands=[]
  while args:

    if len(args)>1 and args[0]=='init': db_name=args[1]

    cmd=available_commands.get(args[0])
    if cmd is 'show':
      if len(args)>1:
        sys.stderr.write("Error: command 'show' must be the last one\n")
        return -1
      show_matplotlib = True
      
    if cmd is None:
      sys.stderr.write("Unrecognized command: %s\n"%args[0])
      usage(me)
      return -1

    import types
    if type(cmd)==types.ClassType:
      cmd=cmd()
    else:
      cmd=getattr(cmd,cmd.__name__.split('.')[-1])()
    args=cmd.parse(args)
    commands.append(cmd)
    

  # Bad idea: let matplotlib decide which backend it uses.
  # ...or you'll get errors on platforms where a given one (such as GTK)
  # is not installed
  #import matplotlib
  #matplotlib.use('GTK')

  for cmd in commands:
    cmd.run()

  if show_matplotlib:
    from matplotlib.pylab import show
    show()

if __name__ == '__main__':
  import sys
  sys.exit(main(sys.argv))
