# -*- coding: iso-8859-1 -*-
## LICENSE
"""
"""
import getopt, sys
from herbs.utils import log
from herbs.app import dataset, evaluations, quality_measures, rules_set
from herbs.app import compatible

class cmd_list:
  """

  Implements command ``list``,
  see `herbs.app.cmd_list.usage`.
  
  """
  list_datasets = False
  list_rulessets = False
  list_compatible = False
  list_measures = False
  list_available_measures = False
  list_evaluations = False
  
  def parse(self,args):
    """

    :return: the remaining, unprocessed arguments

    """
    log.trace("%s"%args)
    #if not args[1:]:
    #  raise ValueError, "command list ..."
    #import pdb ; pdb.set_trace()
    try:
      from getopt import getopt
      options,args = getopt(args[1:], 'hadrceqQ',
                            ['help', 'all', 'datasets', 'rulessets',
                             'compatible','evaluations','quality-measures',
                             'available-quality-measures'])
    # TBD except GetoptError: option %s not recognized
    # TBD attributes in GetoptError: msg, opt
    except:
      self.usage()
      log.debug("Couldn't parse options", exc_info=1)
      raise ValueError, "Unable to parse options. Aborting"

    all=('-a', '--all')
    for k, v in options:
      if k in ('-h', '--help'): self.usage(); continue
      if k in all+('-d', '--datasets'): self.list_datasets = True;
      if k in all+('-r', '--rulessets'): self.list_rulessets = True;
      if k in all+('-c', '--compatible'): self.list_compatible = True;
      if k in all+('-q', '--quality-measures'): self.list_measures = True;
      if k in all+('-Q', '--available-quality-measures'):
        self.list_available_measures = True;
      if k in ('-e', '--evaluations'): self.list_evaluations = True;

  def run(self):
    """
    """
    if self.list_datasets:
      datasets = dataset.datasets()
      print 'Available datasets:'
      if not datasets:
        print 'None'
      else:
        for ds_name in datasets.keys():
          print '   ',ds_name

    if self.list_rulessets:
      rulessets = rules_set.rules_sets()
      print 'Available rulessets:'
      if not rulessets:
        print 'None'
      else:
        for rs_name in rulessets.keys():
          print '   ',rs_name

    if self.list_compatible:
      couples = compatible.compatible_couples()
      print 'Compatible couples (rules set, dataset):'
      if not couples:
        print 'None'
      else:
        for rs,ds in couples:
          print '    (%s, %s)'%(rs, ds)

    if self.list_measures:
      measures = quality_measures.computed_measures()
      print 'Quality measures (quality measure, rules set, dataset):'
      if not measures:
        print 'None'
      else:
        for qual,rs,ds in measures:
          print '    (%s, %s, %s)'%(qual, rs, ds)

    if self.list_evaluations:
      couples = evaluations.available_evaluations()
      print 'Evaluations (rules set, dataset):'
      if not couples:
        print 'None'
      else:
        for rs,ds in couples:
          print '    (%s, %s)'%(rs, ds)

    if self.list_available_measures:
      names=quality_measures.measures.keys()
      names.sort()
      print 'Available quality measures: '
      for name in names:
        print '    %s \t%s'%(name,quality_measures.measures[name])

  def usage(self):
    sys.stderr.write(usage())


def usage():
  """
list [options]

Options
-------
  -h --help                  this help
  -a --all         (default) equivalent to -d -r -c -q -Q
  -d --datasets              list datasets
  -r --rulessets             list rules sets
  -c --compatible            list all compatible couples (rules set, dataset)
  -q --quality-measures      list all computed quality measures
  -Q --available-quality-measures
                             list available quality measures
"""
  # leave this undocumented?
  # -e --compatible            list all caches evaluations
  return usage.__doc__
