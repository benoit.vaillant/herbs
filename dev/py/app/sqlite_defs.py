db_schema=("""
-- Available datasets in an HERBS session are summarized there. Each dataset
-- then stores its data in a dedicated table named DS_<NAME>
CREATE TABLE DATASET (
  id integer PRIMARY KEY,
  name text UNIQUE NOT NULL,-- name for the dataset. It should be a valid SQL
                            -- name (for example, whitespaces are not allowed)
  comments text,
  internals text            -- caches the list of attributes' names
);
""","""

-- The next table holds all available RulesSets within an HERBS session.
-- Corresponding rules are stored within a single table RULE (see below)
CREATE TABLE RULES_SET (
  id integer PRIMARY KEY,
  name text UNIQUE NOT NULL,
  comments text,
  internals text              -- caches the list of attributes' names
);

""","""
-- CREATE TABLE compatibility???
-- Should a COMPATIBILITY table be added? 
-- Probably, yes, please confirm. However, I made the decision not to use
-- it to identify (dataset, ruleset) pairs; former db-schema did that,
-- introducing redundancies in relationships (such as the one joining
-- evaluations to the compatibility table *and* to rules, while rules and
-- rulesets were obviously related and rules sets related to the compatibility
-- table.

""","""
CREATE TABLE RULE (
  id integer PRIMARY KEY,
  FK_RULES_SET integer NOT NULL, --references its rules set
  head text NOT NULL,
  tail text NOT NULL,
  FOREIGN KEY (FK_RULES_SET) REFERENCES RULES_SET(id)
);

""","""
CREATE TABLE EVALUATIONS (
  FK_DataSet integer NOT NULL,-- references a dataset
  FK_Rule integer NOT NULL,   -- references a rule ; both fields make a PK
  FK_RULES_SET integer NOT NULL, --references its rules set (copy)
  na integer NOT NULL,        -- nb of elements satisfying the head of the rule
  nb integer NOT NULL,        -- nb of elements satisfying the tail of the rule
  nab integer NOT NULL,       -- nb of elements satisfying the rule
  nab_ integer NOT NULL,      -- nb of counter-examples (==nab-na)
  PRIMARY KEY (FK_DataSet, FK_Rule),
  FOREIGN KEY (FK_Rule) REFERENCES RULE(id),
  FOREIGN KEY (FK_Dataset) REFERENCES DATASET(id)
  FOREIGN KEY (FK_RULES_SET) REFERENCES RULES_SET(id)
);

""","""
CREATE TABLE QUALITY_MEASURES (
  id integer PRIMARY KEY,
  name text NOT NULL,          -- PK, name of the quality_measures, as
                               -- defined in herbs' module quality_measures
  FK_Rule integer NOT NULL,    -- references a rule
  FK_DataSet integer NOT NULL, -- references a dataset
  FK_RULES_SET integer NOT NULL, --references its rules set (copy)
  value double,       -- cached calculated value
  FOREIGN KEY (FK_Rule) REFERENCES RULE(id),
  FOREIGN KEY (FK_DataSet) REFERENCES DATASET(id)
  FOREIGN KEY (FK_RULES_SET) REFERENCES RULES_SET(id)
);


""","""
-- This index is specially designed to accelerate the computation of IPD
-- in herbs.app.quality_measures.evaluate_IPD()
-- On my machine, the computation takes approx. the same time as the
-- computation of the other measures, that is about 30s.  Without it, it takes
-- more than 4 hours! (w/ quality_measure having 840,000 rows)
CREATE INDEX QM_DS_R_N ON QUALITY_MEASURES (FK_Dataset, FK_Rule, name);

""","""
-- This one helps herbs.app.quality_measures.is_computed()
CREATE INDEX QM_NAME ON QUALITY_MEASURES (name);

""","""
-- Accelerates quality_measures.has_computed_measures() when the answer is
-- False
CREATE INDEX QM_RS_DS ON QUALITY_MEASURES (FK_RULES_SET, FK_DATASET);

""","""
CREATE TRIGGER rules_set_delete_trigger DELETE ON RULES_SET
  FOR EACH ROW
  BEGIN
      DELETE FROM RULE WHERE FK_RULES_SET = old.id;
  END;

""","""
CREATE TRIGGER rule_delete_trigger DELETE ON RULE
  FOR EACH ROW
  BEGIN
      DELETE FROM EVALUATIONS      WHERE FK_RULE = old.id;
      DELETE FROM QUALITY_MEASURES WHERE FK_RULE = old.id;
  end;

""","""
CREATE TRIGGER dataset_delete_trigger DELETE ON DATASET
  FOR EACH ROW
  BEGIN
    DELETE FROM EVALUATIONS      WHERE FK_DATASET = old.id;
    DELETE FROM QUALITY_MEASURES WHERE FK_DATASET = old.id;
    -- Do NOT DROP TABLE DS_<old.name>, this must be done explicitly
  END;

""")



"""
-- Sample DS table
-- 
-- Note that there is no PK is defined
-- this is intended, there is no need to identify unique rows in the DS
-- within the scope of HERBS.
-- 
-- CREATE TABLE DS_abalone (
--   Sex TEXT default NULL,
--   Length float default NULL,
--   Diameter float default NULL,
--   Height float default NULL,
--   WholeWeight float default NULL,
--   ShuckedWeight float default NULL,
--   VisceraWeight float default NULL,
--   ShellWeight float default NULL,
--   Rings int(11) default NULL
-- );
"""
