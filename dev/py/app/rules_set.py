# -*- coding: iso-8859-1 -*-
## LICENSE
"""
A rule: id (int), head (attr=value [and attr=value]*), tail (attr=value)
  accessible via __getitem__(): rule['id']

:group Manipulating rules set: create_rules_set,delete_rules_set,rename_rules_set,rules_sets,exists,attributes,rules_named,nb_rules,_id,_rules_id_named,_rules_set_for_rule_id
:group Importing files:import_apriori,import_c45,generic_rules_import
:group Parsing ``C45`` files: related methods: generic_import_c45,parse_tree,analyse_c45_line,split_c45_head
:group Parsing ``apriori`` files: related methods: generic_import_apriori,parse_apriori_line

"""
#from __future__ import generators 
import re
import conn
from herbs.utils import log, FileFormatError, openext

##
## rules set
##
def import_ruleset(name, file, comments=""):
  """
  Imports a rules set

  :Parameters:
    - `name`: name of the set. It should be unique.
    - `file`:
    - `comments`: (optional)

  :except ValueError: 
  """
  create_rules_set

def create_rules_set(name, handle_txn=True):
  """
  Create a rules set in the application's database

  :Parameters:
    - `name`: name of the rules set to create
    - `handle_txn`: (default: True) if True, the method commit() or rollback()
      the underlying database connection upon success or failure. If False, the
      transaction is left untouched and it is the caller's responsability to
      handle it.

  :see: herbs.app.conn

  :return: the ``id`` corresponding to the new row that has been added in the
    database
  """
  comments="TBD"

  if exists(name): # TBD: test unit
    raise ValueError, "A rule set named %s already exists"%name

  cur=conn.cursor()
  try:
    create='INSERT INTO RULES_SET (id, name, comments) '\
            'VALUES (NULL,"%s","%s")'%(name,comments)
    cur.execute(create) #_sqlite.IntegrityError in case non-uniqueness for name

    #rules_set_id=conn.cnx.db.sqlite_last_insert_rowid()
    
  except:
    handle_txn and conn.rollback()
    raise
  else:
    handle_txn and conn.commit()

  rules_set_id=cur.execute('SELECT last_insert_rowid()')
  #import pdb; pdb.set_trace()
  rules_set_id=rules_set_id.fetchone()[0];
  return rules_set_id

def delete_rules_set(name):
  """
  Deletes a rules set from the application's database

  :raise ValueError: if no rules set named ``name`` exists

  """
  
  cur=conn.cursor()
  # CASCADE DELETE: BdR, BdC_BdR, etc.
  try:
    id=_id(name)
    cur.execute("DELETE FROM RULES_SET WHERE id==%li"%id)
  except:
    conn.rollback()
    raise
  else:
    conn.commit()

def rename_rules_set(name, new_name):
  """
  """
  cur=conn.cursor()
  # CASCADE DELETE: BdR, BdC_BdR, etc.
  try:
    id=_id(name)
    cur.execute('UPDATE RULES_SET SET name="%s" WHERE id=%li'%(new_name,id))
  except:
    conn.rollback()
    raise
  else:
    conn.commit()


def rules_sets():
  """

  Returns the set of available rules sets, as a dictionary where keys are the
  rules sets' names and the values are dictionaries mapping ``id`` and
  ``comments`` to their corresponding values.

  Example: ``{ 'cars': {'id':1, comments:'How people choose their car'} }``

  """
  # field 'internals' is deliberately ignored here
  cur = conn.cursor()
  cur.execute('SELECT id, name, comments FROM RULES_SET')
  res = cur.fetchall()
  d = {}
  [ d.setdefault(name, { 'id':id, 'comments':comments } )
    for id,name,comments in res ]
  return d

def exists(name):
  """
  Tells if such a rules set exists
  """
  cur=conn.cursor()
  cur.execute("SELECT id FROM RULES_SET WHERE NAME='%s'"%name)
  res=cur.fetchall()
  return not not res
  
def _id(name):
  """
  Returns the id for the rules set `name`.
  :except ValueError: if no such dataset exists
  """
  if name is None:
    raise ValueError, "None is an incorrect name"
  cur=conn.cursor()
  cur.execute("SELECT id FROM RULES_SET WHERE NAME='%s'"%name)
  res=cur.fetchall()
  if not res:
    raise ValueError, "Ruleset %s does not exist"%name
  assert len(res)==1
  return res[0][0]

  
def attributes(rulesset_name):
  """
  Returns the set of attributes' names used in the requested rules set

  :param rulesset_name: name of the rules set
  :exception ValueError: if no such rules set exists$
  """
  cur=conn.cursor()
  cur.execute("SELECT internals FROM RULES_SET WHERE NAME='%s'"%rulesset_name)
  res=cur.fetchone()
  if not res:
    raise ValueError, "Ruleset %s does not exist"%rulesset_name
  return eval(res[0])

##
## Rules
##
def make_rule(head, tail, id=None):
  "Builds a new rule"
  from herbs.app.Rule import Rule
  return Rule(head, tail, id)
  
def _rules_id_named(dataset_name, limit=0):
  """Returns the list of ids for the rules in dataset `dataset_name`

  :Parameters:
    - `dataset_name`: name of the dataset (!)

    - `limit`: (optional, default: 0) the maximum number of ids to return, or
      unlimited if unspecified or ``0`` (zero)
    
  :returns: a list of integers, sorted by ascending order.

  :exception ValueError: if no dataset with that `dataset_name` exists
  """
  cur = conn.cursor()
  sql = 'SELECT id FROM RULE WHERE FK_RULES_SET=%li ORDER BY id'
  if limit:
    sql += ' LIMIT %li'%limit
  cur.execute(sql%_id(dataset_name))
  return [id[0] for id in cur.fetchall()]

def _rules_set_for_rule_id(rule_id):
  """
  Returns the rules set's name for the supplied rule.

  :param rule_id: intern identifier for the rule
  :returns:

  :exception RuntimeError: if no such rules set can be foiund; this may be
    because no such rule exists, or because the corresponding rules sets has
    been deleted. Anyhow this should not happen --we do not distinguish both
    cases since this is a private method normally called in a controlled
    environment within the framework.
  
  """
  sql="SELECT RULES_SET.name from RULES_SET WHERE RULES_SET.id = (SELECT FK_RULES_SET FROM RULE WHERE id = %li )" % rule_id
  cur = conn.cursor()
  try:
    cur.execute(sql)
    res=cur.fetchall()
  except:
    conn.rollback()
    raise
  if not res:
    raise RuntimeError, 'Unable to find a rules set for rule id: %li'
  assert len(res)==1
  return res[0][0]

def rules_named(name):
  "Returns the set of rules named `name`"
  cur=conn.cursor()
  
  limit=10000
  offset=0
  sql='SELECT head,tail,id FROM RULE WHERE FK_RULES_SET=%li ORDER BY id LIMIT %li OFFSET %li'
  rs_id=_id(name)
  cur.execute(sql%(rs_id, limit,offset))
  rules=cur.fetchall()

  ok=1
  #return cur.fetchall()
  while ok:
    ok=(cur.rowcount==limit)
    for r in rules:
      yield make_rule(*r)
    offset+=limit
    cur.execute(sql%(rs_id,limit,offset))
    rules=cur.fetchall()

  rules=[make_rule(*r) for r in rules if r is not []]
    
def nb_rules(name):
  """
  Returns the number of rules stored in a rules set

  :param name: name of the rules set
  :exception ValueError: if no such rules set exists
  """
  id = _id(name)
  cur=conn.cursor()
  cur.execute("SELECT COUNT(*) FROM RULE WHERE FK_RULES_SET=%li" % id)
  return int(cur.fetchone()[0])

def generic_rules_import(filename, format, name, *args):
  """
  Imports any
  This method creates the appropriate rules'set then it loads the rules into
  the database.

  It invokes the method `create_rules_set`, hence this method should not be
  called to create the rules set.

  :Parameters:
    - `filename`: 
    - `format`: either ``'c45'`` or ``'apriori'``
    - `name`:

  """
  if format=='c45':
    func=generic_import_c45
  elif format=='apriori':
    func=generic_import_apriori
  else:
    raise ValueError, 'Unrecognized format: %s'%format

  cur=conn.cursor()
  try:
    rules_set_id=create_rules_set(name, handle_txn=False)
    insert='INSERT INTO RULE (id, FK_RULES_SET, head, tail) VALUES (NULL, %i, "%%(head)s", "%%(tail)s");'%rules_set_id
    import sys
    insert_fct=lambda rule: rule and cur.execute(insert%rule)
    
    attributes_names=func(filename, insert_fct, *args)

    cur.execute('UPDATE RULES_SET SET internals="%s" WHERE id=%li'%(str(attributes_names), rules_set_id))

  except:
    conn.rollback()
    raise
  else:
    conn.commit()

  return attributes_names

##
## Parsing of apriori output files
##
apriori_attr_regexp=re.compile("([A-Za-z0-9_]+='?[^']+'?)+")
def import_apriori(filename, name):
  """
  """
  generic_rules_import(filename,'apriori', name)
  
def generic_import_apriori(filename, rule_fct, discard_no_head=True):
  """
  Parses the given file, executing rule_fct for each rule

  :Parameters:

    - `filename`: full path to the file to import

    - `rule_fct`: a function accepting one parameter: a rule; this function is
      executed for each rule parsed in the file. If `discard_no_head` is true,
      this function should be ready to accept None value in place of the
      discarded rules.

    - `discard_no_head`: if true (default), the rules with no head are
      discarded and the function returns None.

  :return: the list of attributes' names used in the imported rules set
  """
  attributes=[]
  f=openext(filename)
  line=f.readline()
  line_nb=0
  import time
  t0=time.time()
  try:
    while line:
      line.strip()
      if not line: continue
      line_nb+=1
      if not line_nb%10000:
        log.debug("reading line %s after %.3fs",line_nb,time.time()-t0)
      rule_fct(parse_apriori_line(line, attributes, discard_no_head))
      line=f.readline()
  finally:
    f.close()

  attributes.sort()
  return attributes
  
def parse_apriori_line(line, attributes, discard_no_head=True):
  """Parse a line in 'apriori' format and returns TBD

  :Parameters:
    - `line`: the line to parse

    - `discard_no_head`: if true (default), the rules with no head are
      discarded and the function returns None.

    - `attributes`: the names of the attributes used in the current `line`
      will be appended to that list --attributes in the current line that are
      already in the list are **not** appended again.

  """
  #import pdb ; pdb.set_trace()
  
  tail, _head=line.split(' <- ')
  #head=' and '.join(apriori_attr_regexp.findall(head))
  _head=_head.split()[:-2] # removing trailing (6.2%, 63.9%)
  head=' and '.join(_head)
  if not head and discard_no_head:
    return None
  attrs=tail.split('=')[:1]+[equality.split('=')[0] for equality in _head]
  attributes+=[attr for attr in attrs if attr not in attributes]
  return make_rule(head, tail)

##
## parsing of c4.5 files
## TBD: c4.5 parsing of references like: [S1] in files
##
def import_c45(filename, name, tail_attr_name):
  generic_rules_import(filename, 'c45', name, tail_attr_name)

def generic_import_c45(filename, rule_fct, tail_attr_name):
  """
  Parses the given file, executing rule_fct for each rule

  :Parameters:
    - `filename`: full path to the file to import

    - `rule_fct`: a function accepting one parameter: a rule; this function is
      executed for each rule parsed in the file.

    - `tail_attr_name`: name of the attribute used in the tail of the rules

  :raise FileFormatError:
  
  :return: the list of attributes' names used in the imported rules set
  """
  attrs=[tail_attr_name]
  f=openext(filename)
  line=f.readline()
  marker=None
  try:
    while line:
      line.strip()
      if line[-1]=='\n': line=line[:-1]
      # search for the marker
      if line=='Decision Tree:':
        marker=1
        break
      line=f.readline()

    if not marker:
      raise FileFormatError, "Couldn't find marker"
    
    line=f.readline() # skip the next empty line


    rules, pending_rules = parse_tree(f, tail_attr_name, attrs)

    for rule in rules:
      rule_fct(rule)


    subtree_marker="Subtree [S"
    # At this point we've read the main tree, now read the subtrees
    while pending_rules and line:

      line=line.strip()

      if "Simplified Decision Tree" in line or line=="Tree saved":
        break # end of parsing

      # find the first sub-tree marker
      if not line[:len(subtree_marker)] == subtree_marker:
        line = f.readline()
        continue
      
      subtree_ref = line[len(subtree_marker):-1]
      try:
        subtree_ref=int(subtree_ref)
      except ValueError:
        raise FileFormatError, "Unable to parse reference %s"%subtree_ref

      log.trace('Processing subtree S%i', subtree_ref)
      line=f.readline() # skip next empty line

      pending=pending_rules.get(subtree_ref)
      if not pending:
        log.warn('Got a description for subtree S%i with no reference in the main tree')
        continue
      
      rules, unhandled_pending = parse_tree(f, tail_attr_name, attrs,
                                            pending['head_terms'])

      if unhandled_pending:
        raise FileFormatError, "Unsupported: Subtree S%i references its own subtree(s)"%subtree_ref

      for rule in rules:
        rule_fct(rule)

      line=f.readline()
    
  finally:
    f.close()

  return attrs

def parse_tree(file, tail_attr_name, attributes,
               additional_head_terms=None):
  """

  Parses a whole C45 tree (either the main one or a subtree) and returns the
  list of extracted rules, plus a dictionary mapping sub-trees' references to
  there partially defined rule.

  You should never need to call this method, it is for internal use mostly.

  :Parameters:

    - `file`: a stream positioned to the beginning of a subtree. Please note
      that we really mean that: for example, if the stream is positioned
      before the tree with one or more empty lines, the method will
      immediately return.
    
    - `tail_attr_name`: name of the attribute being the tail/conclusion of the
      rule

    - `attributes`: passed as-is to `analyse_c45_line()`, see this method for
      details.


  return: [rules], {pending_rules}
  """
  f=file
  rules = []
  level = rule = None
  pending_rules = {}
  head_terms = []
  
  line=f.readline()
  while line:
    line=line.strip()
    if not line \
       or "Simplified Decision Tree" in line or line=="Tree saved":
      break # end of parsing
  
    oldlevel, oldrule=level, rule
    
    level, rule, pending = analyse_c45_line(line, head_terms,
                                            tail_attr_name, attributes,
                                            additional_head_terms)
    if pending:
      log.trace('Detected pending rule: %s -> %s', pending[0], pending[1])
      pending_rules[pending[0]] = pending[1]
      
    if oldlevel and level<=oldlevel and oldrule is None:
      # previous line was not a node, the level should have been oldlevel+1
      raise FileFormatError, "Wrong format"
  
    if rule:
      rules.append(rule)

    if pending:
      rule="A pending rule should be considered as a node in the next "\
           "iteration, see test, above"
  
    line=f.readline()
  return rules, pending_rules

def analyse_c45_line(line, head_terms, tail_attr_name, attributes,
                     additional_head_terms):
  """
  Analyses a C45 line.

  Note: in the returned ``rule``, every value is quoted (surrounded by simple
  quotes); this allows us to directly use the expression into a SQL WHERE
  clause.

  :Parameters:
    - `line`: the line to analyze

    - `head_terms`: the list of tuples (attr, operator, value) that were
      extracted from the previous lines in the C45 file. If the supplied
      `line` is not a node, the extracted attribute, operator and value are
      appended to this list.

    - `tail_attr_name`: name of the attribute being the tail/conclusion of the
      rule

    - `attributes`: the names of the attributes used in the current `line`
      will be appended to that list --attributes in the current line that are
      already in the list are **not** appended again.

    - `additional_head_terms`: a list of terms, as in `head_terms`, that
      should be systematically added to the `head_terms` when producing a
      rule --this is used when processing subtrees.

  :return: level, rule, pending_rule

  """
  log.trace("analysing: %s", line)
  s=line.split('|')
  level=len(s)-1
  if level > len(head_terms):
    raise FileFormatError, "Wrong format: %s"%line
  
  for i in range(len(head_terms)-level):
    head_terms.pop()
  
  info=s[-1].strip().split(':')
  log.trace("info: %s",info)

  attr, operator, value = split_c45_head(info[-2]) #info[-2].split(' = ')

  if attr not in attributes:
    attributes.append(attr)

  rule = pending = None

  if not info[-1]: # this is not a node
    head_terms.append((attr, operator, value))
  else: # this is a node
    head=''
    if not additional_head_terms:
      additional_head_terms = []
    for a, o, v in additional_head_terms + head_terms:
      head+="%s%s'%s' and "%(a, o, v)
    head+="%s%s'%s'"%(attr, operator, value)

    tail_value=info[-1].split('(')[0].strip()
    if tail_value=='[S??]':
      raise FileFormatError, "Unable to read c45 file: %s"%line
    if tail_value[:2]=='[S':
      ref = tail_value[2:-1]
      try:
        nb_pending = int(ref)
      except ValueError:
        raise FileFormatError, "Unable to parse reference %s"%(tail_value,)
      head_terms.append((attr, operator, value))
      pending = (nb_pending, {'level': level, 'head_terms': list(head_terms)} )
    else:
      log.trace("%s %s",tail_attr_name, tail_value)
      tail=tail_attr_name + "='" + tail_value +"'"
      log.trace("rule: %s := %s",head, tail)
      
      rule=make_rule(head, tail)
    
  return level, rule, pending


def split_c45_head(line):
  """
  Extracts and returns attribute, operator and value from a c4.5 'head',

  :Parameters:
    - `line` the so-called 'head' in a c4.5 line, i.e. a string made of
      an attribute name, an operator (``<=``,``>=``,``<``,``>``,``=``) and a
      value, like in:  ``Height <= 0.11``
  """
  operators=('<=','>=','<','>','=')
  idx=-1
  for op in operators:
    idx=line.find(op)
    if idx != -1:
      break

  if idx==-1:
    raise FileFormatError, "No operator found in line: %s"%line

  return line[:idx].strip(),op,line[idx+len(op):].strip()
