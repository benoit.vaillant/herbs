#! /usr/bin/env python
# -*- coding: iso-8859-1 -*-
## LICENSE
"""
Tests for evaluations
"""
import unittest
if __name__ == "__main__":
  import utils, sys
  utils.fixpath()

from herbs.app import evaluations, dataset, rules_set
from herbs.app import conn
from herbs.app.dataset import TEXT

class Test_Evaluations(unittest.TestCase):
  """
  Tests for module evaluation
  """
  def setUp(self):
    conn.init('test.db', create=True)
    self.cur=conn.cursor()
    dataset.import_csv('data/abalone_with_headers.csv','abalone',',')
    rules_set.import_apriori('data/rules.apriori', 'glop')
    
  def tearDown(self):
    del self.cur
    import os
    os.unlink('test.db')

  def test_01_evaluate(self):
    "[evaluations] evaluate"
    self.assertRaises(ValueError, evaluations.evaluate, 'rs_car','ds_car')

    from herbs.app import rules_set, dataset
    dataset.import_csv( 'data/car.data', 'ds_car', ',',
                            ( 'buying','maint','doors','persons','lug_boot',
                              'safety','offer'),
                            (TEXT,)*7 )

    self.assertRaises(ValueError, evaluations.evaluate, 'rs_car','ds_car')

    rules_set.import_c45('data/car.c45', 'rs_car', 'offer')
    rules_set.import_apriori('data/rules.apriori', 'car.apriori')

    # TBD we only check that this does not raise... not that it works as
    # TBD expected --> this still needs to be validated, i.e. we need a
    # TBD test sample that we could compare to.
    evaluations.evaluate('rs_car','ds_car')
    evaluations.evaluate('car.apriori','ds_car')

    # Those two are not compatible
    self.assertRaises(ValueError, evaluations.evaluate, 'rs_car', 'abalone')

  def test_02_is_evaluated(self):
    "[evaluations] is_evaluated & evaluations"
    self.assertRaises(ValueError, evaluations.is_evaluated, 'rs_car', 'ds_car')
    self.failIf(evaluations.available_evaluations())

    # import a dataset
    from herbs.app import rules_set, dataset
    dataset.import_csv( 'data/car.data', 'ds_car', ',',
                            ( 'buying','maint','doors','persons', 'lug_boot',
                              'safety','offer'), (TEXT,)*7 )
    # the rules set isn't defined
    self.assertRaises(ValueError, evaluations.is_evaluated, 'rs_car', 'ds_car')

    rules_set.import_c45('data/car.c45', 'rs_car', 'offer')

    # both rulesset and dataset are now loaded
    self.failIf(evaluations.is_evaluated('rs_car', 'ds_car'))
    self.failIf(evaluations.available_evaluations())

    evaluations.evaluate('rs_car','ds_car')
    self.failUnless(evaluations.is_evaluated('rs_car', 'ds_car'))
    self.assertEqual(1, len(evaluations.available_evaluations()))
    self.failUnless(('rs_car', 'ds_car') \
                    in evaluations.available_evaluations())

  def test_16_evaluation(self):
    "[evaluations] evaluation"
    # import a rules set/dataset
    from herbs.app import rules_set, dataset
    dataset.import_csv( 'data/car.data', 'ds_car', ',',
                        ( 'buying','maint','doors','persons', 'lug_boot',
                          'safety','offer'), (TEXT,)*7 )
    rules_set.import_c45('data/car.c45', 'rs_car', 'offer')
    
    # both rulesset and dataset are now loaded
    evaluations.evaluate('rs_car','ds_car')
    ds_id=dataset._dataset_id('ds_car')
    rules = rules_set.rules_named('rs_car')
    for r in rules:
      self.failUnless(evaluations.evaluation(r['id'], ds_id).has_key('na'))
      self.failUnless(evaluations.evaluation(r['id'], ds_id).has_key('nb'))
      self.failUnless(evaluations.evaluation(r['id'], ds_id).has_key('nab'))
      self.failUnless(evaluations.evaluation(r['id'], ds_id).has_key('nab_'))
    
def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(Test_Evaluations, "test_"))
    return suite

if __name__ == "__main__":
    errs = utils.run_suite(test_suite())
    sys.exit(errs and 1 or 0)
