#! /usr/bin/env python
# -*- coding: iso-8859-1 -*-
## LICENSE
"""
Tests for dataset

"""
import unittest
if __name__ == "__main__":
  import utils, sys
  utils.fixpath()

from herbs.app import dataset
from herbs.app.dataset import TEXT, INTEGER, FLOAT
from herbs.app import conn
from herbs.utils import FileFormatError

class Test_Dataset(unittest.TestCase):
  """
  Tests for the dataset
  """
  def setUp(self):
    conn.init('test.db', create=True)
    self.cur=conn.cursor()
    
  def tearDown(self):
    del self.cur
    import os
    os.unlink('test.db')
    
  def test_01_guess_attribute_type(self):
    "[dataset] guess_attribute_type"
    self.assertEqual('integer', dataset.guess_attribute_type('4'))
    self.assertEqual('text', dataset.guess_attribute_type('412L'))
    self.assertEqual('float', dataset.guess_attribute_type('4.'))
    self.assertEqual('float', dataset.guess_attribute_type(' 04.12\n'))
    self.assertEqual('text', dataset.guess_attribute_type(' abc '))

  def test_02_guess_attributes_types(self):
    "[dataset] guess_attributes_types"
    import bz2
    abalone=bz2.BZ2File('data/abalone.data.bz2')
    attrs_types=dataset.guess_attributes_types(abalone, ',', False)
    self.assertEqual(9, len(attrs_types))
    self.assertEqual(['text']+['float']*7+['integer'], attrs_types)
    abalone.close()

    abalone=open('data/abalone.bad.csv')
    self.assertRaises(FileFormatError,
                      dataset.guess_attributes_types, abalone, ',', False)
    abalone.close()
    
    abalone=open('data/abalone.2.csv')
    attrs_types=dataset.guess_attributes_types(abalone, ',', False)
    self.assertEqual(['text']+['float']*7+['integer'], attrs_types)
    abalone.close()

    # Test w/ headers
    abalone=open('data/abalone_with_headers.csv')
    attrs_types=dataset.guess_attributes_types(abalone, ',', True)
    self.assertEqual(['text']+['float']*7+['integer'], attrs_types)
    abalone.close()

    
  def test_03_create_delete_dataset_and_id(self):
    "[dataset] create_dataset, delete_dataset, exists, _dataset_id"
    self.failIf(dataset.exists('test_03', check_dataset_table=True))

    # both lists should have the same length
    self.assertRaises( ValueError, dataset.create_dataset,
                       'test_03','', ('Sex', 'Height', 'WholeWeight','Rings'),
                       (TEXT, FLOAT, FLOAT) )

    dataset.create_dataset('test_03','',
                           ('Sex', 'Height', 'WholeWeight','Rings'),
                           (TEXT, FLOAT, FLOAT, INTEGER))
    self.failUnless(dataset.exists('test_03', check_dataset_table=True))
    self.failUnless(dataset._dataset_id('test_03')==1)

    # a dataset with the same name is already registered
    self.assertRaises( ValueError, dataset.create_dataset,
                       'test_03','', ('Sex', 'Height', 'WholeWeight','Rings'),
                       (TEXT, FLOAT, FLOAT, INTEGER) )

    dataset.delete_dataset('test_03')
    self.failIf(dataset.exists('test_03', check_dataset_table=True))

  def test_04_import_csv(self):
    "[dataset] import_csv & dataset"
    self.assertEqual(10, dataset.import_csv('data/abalone_with_headers.csv','abalone',','))
    self.assertEqual(10, len(dataset.dataset('abalone')))

  def test_05_attributes(self):
    "[dataset] attributes & attributes_names"
    self.assertRaises(ValueError, dataset.attributes, 'does_not_exist')
    attrs=['Sex', 'Length', 'Diameter', 'Height', 'Whole_weight',
           'Shucked_weight', 'Viscera_weight', 'Shell_weight', 'Rings']
    types=['text', 'float', 'float', 'float', 'float', 'float', 'float',
           'float', 'integer']
    
    dataset.import_csv('data/abalone.csv','abalone',',', attrs)
    self.assertEqual(map(None,attrs,types), dataset.attributes('abalone'))
    self.assertEqual(attrs, dataset.attributes_names('abalone'))

  def test_06_datasets(self):
    "[dataset] datasets"
    self.assertEqual({}, dataset.datasets())
    dataset.import_csv('data/abalone_with_headers.csv','abalone',',')
    attrs=['buying','maint','doors','persons','lug_boot','safety','offer']
    dataset.import_csv('data/car.data', 'car', ',', attrs)
    self.assertEqual(2, len(dataset.datasets()))
    self.failUnless('car' in dataset.datasets().keys())
    self.failUnless('abalone' in dataset.datasets().keys())
    
  def test_07__dataset_id_n_co(self):
    "[dataset] _dataset_id & _dataset_name_for_id"
    self.assertRaises(ValueError, dataset._dataset_id, 'foobar')
    self.assertRaises(ValueError, dataset._dataset_name_for_id, 999)

    dataset.import_csv('data/abalone_with_headers.csv','abalone',',')
    self.assertEqual('abalone', dataset._dataset_name_for_id(dataset._dataset_id('abalone')))

  def test_08_nb_instances(self):
    "[dataset] nb_instances"
    self.assertRaises(ValueError, dataset.nb_instances, 'foobar')

    dataset.import_csv('data/abalone_with_headers.csv','abalone',',')
    self.assertEqual(10, dataset.nb_instances('abalone'))

def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(Test_Dataset, "test_"))
    return suite

if __name__ == "__main__":
    errs = utils.run_suite(test_suite())
    sys.exit(errs and 1 or 0)

