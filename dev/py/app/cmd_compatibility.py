# -*- coding: iso-8859-1 -*-
## LICENSE
"""
"""
import csv, getopt, sys
from herbs.utils import log
from herbs.app import evaluations, quality_measures, rules_set
from herbs.app import compatible

class cmd_compatibility:
  """

  Implements command ``compatibility``,
  see `herbs.app.cmd_compatibility.usage`.
  
  """
  list_compatible = False
  compute_eval = False
  compute_all_evals = False
  quality_measure = False
  all_quality_measures = False

  show_eval = False
  show_measure = False
  

  def parse(self,args):
    """

    :return: the remaining, unprocessed arguments

    :except ValueError: if ``len(args)<2`` (the command needs a file path), or
      if there are unrecognized options.
    """
    log.trace("%s"%args)
    #if not args[1:]:
    #  raise ValueError, "command compatibility ..."
    #import pdb ; pdb.set_trace()
    try:
      from getopt import getopt, GetoptError
      options,args = getopt(args[1:], 'hle:Eq:Qs:S:',
                            ['help', 'list', 'evaluation=', 'all-evaluations', 
                             'quality-measure=', 'all-quality-measures',
                             'show-eval=', 'show-quality-measure='])
    # TBD except GetoptError: option %s not recognized
    # TBD attributes in GetoptError: msg, opt
    except GetoptError, exc:
      self.usage()
      log.debug("Couldn't parse options", exc_info=1)
      log.error(exc.msg)
      raise
    
    compute_eval = quality_measure = show_eval = show_measure = None
    for k, v in options:
      if k in ('-h', '--help'): self.usage(); continue
      if k in ('-l', '--list'): self.list_compatible = True; continue

      if k in ('-e', '--evaluate'): compute_eval = v; continue
      if k in ('-q', '--quality-measure'): quality_measure = v; continue

      if k in ('-E', '--all-evaluations'):
        self.compute_all_evals = True
        continue
      if k in ('-s', '--show-eval'): show_eval = v; continue
      if k in ('-S', 'show-quality-measure'): show_measure = v; continue
      if k in ('-Q', '--all-quality-measures'):
        self.all_quality_measures = True
        continue

    if quality_measure == 'help':
      print 'Available quality measures: '
      names=quality_measures.measures.keys()
      names.sort()
      for name in names:
        print '%s:\t%s'%(name,quality_measures.measures[name])
      quality_measure = None

    if compute_eval:
      self.compute_eval = compute_eval.split(':')
      if len(self.compute_eval) != 2:
        log.error("Invalid parameter for --evaluate: %s",compute_eval)
        raise ValueError, "Invalid parameter for --evaluate: %s"%compute_eval
      
    if show_eval:
      self.show_eval = show_eval.split(':')
      if len(self.show_eval) != 2:
        log.error("Invalid parameter for --show-eval: %s",show_eval)
        raise ValueError, "Invalid parameter for --show-eval: %s"%show_eval
      
    if quality_measure:
      self.quality_measure = quality_measure.split(':')
      if len(self.quality_measure) != 3:
        log.error("Invalid parameter for --quality_measure: %s",quality_measure) 
        raise ValueError, \
              "Invalid parameter for --quality_measure: %s"%quality_measure
      
    if show_measure:
      self.show_measure = show_measure.split(':')
      if len(self.show_measure) != 3:
        log.error("Invalid parameter for --show-measure: %s", show_measure)
        raise ValueError,"Invalid parameter for --show-measure: %s"%show_measure

  def run(self):
    """
    """
    if self.quality_measure and self.all_quality_measures:
      self.quality_measure = False
    
    if self.compute_eval and self.compute_all_evals:
      self.compute_eval = False
    
    if self.list_compatible:
      couples=compatible.compatible_couples()
      print 'Compatible couples (rules set, dataset):'
      if not couples:
        print 'None'
      else:
        for rs,ds in couples:
          print rs, '\t', ds

    if self.compute_eval:
      evaluations.evaluate(*self.compute_eval)

    if self.quality_measure:
      quality_measures.evaluate(*self.quality_measure)

    if self.compute_all_evals or self.all_quality_measures:
      compatible_couples=compatible.compatible_couples()

    if self.compute_all_evals:
      for rs, ds in compatible_couples:
        evaluations.evaluate(rs, ds)

    if self.all_quality_measures:
      for measure in quality_measures.measures.keys():
        for rs, ds in compatible_couples:
          quality_measures.evaluate(measure,rs, ds)

    if self.show_eval:
      evals=evaluations.evals(*self.show_eval)
      print 'Evaluations for (rules set, dataset):'
      if not evals:
        print 'None'
      else:
        # head, tail, na, nb, nab, nab_
        cw = csv.writer(sys.stdout)
        cw.writerow(['head', 'tail', 'na', 'nb', 'nab', 'nab_'])
        for row in evals:
          row=list(row) # sqlite.main.PgResultSetConcreteClass to list
          cw.writerow(row[4:]+row[:4])
    
    if self.show_measure:
      measures = quality_measures.get_measures(*self.show_measure)
      if not measures:
        print 'None'
      else:
        # head, tail, measure
        cw = csv.writer(sys.stdout)
        cw.writerow(['head', 'tail', 'value'])
        for (d,v) in measures:
          cw.writerow([d['head'], d['tail'], v])

  def usage(self):
    sys.stderr.write(usage())


def usage():
  """
compatibility [options]

Misc.
-----
  -h --help             this help
  -l --list             list all compatible couples (rules set, dataset)

Computations
------------
  -e --evaluations <rules_set:dataset>
                        computes and caches evaluation for (rules set, dataset)

  -E --all-evaluations  computes and caches evaluations for all compatible
                        couples

  -q --quality-measure <quality measure:rules_set:dataset>
                        computes and caches values for the supplied quality
                        measure and couple (DS,RS)
                        ('-q help' for a list of available measures)

                        Implies: -e <rules_set:dataset>

  -Q --all-quality-measures
                        computes and caches all quality measures for all
                        compatible couples (rules set, dataset)

                        Implies: -E <rules_set:dataset>

Showing computed values
-----------------------

  -s --show-evaluations <rules_set:dataset>

                        Prints the evaluations for the rules set on a
                        given dataset. Printed values are, for each rule
                        r=-(head, tail) 

  -S --show-quality-measure <measure:rules_set:dataset>
  
"""
  return usage.__doc__
