# -*- coding: iso-8859-1 -*-
## LICENSE
"""
"""
import conn

# matplotlib: no way to save postscript w/ unicode, yet
#x_axis_info = { 'na'  : ( u'taille de la pr�misse', r'$n_a$'),
x_axis_info = { 'na'  : ( 'taille de la premisse', r'$n_a$'),
                'nb'  : ( 'taille de la conclusion', r'$n_b$'),
                'nab' : ( "nombre d'exemples", r'$n_{ab}$'),
                'nab_': ( 'nombre de contre-exemples', r'$n_{a\neg b} $'),
                }
figure_num=1
def distribution(rulesset_name, dataset_name, x_axis, y_axis,
                 logx=False, logy=False, filename=None, show_grid=False,
                 plot_style=',', fig=None):
  """
  """
  global figure_num
  import quality_measures, rules_set, dataset

  x_axis_isa_measure = 0
  if x_axis not in x_axis_info.keys():
    if x_axis in quality_measures.measures.keys():
      x_axis_isa_measure = 1
      x_axis_label = x_axis
      # evaluate, if it hasn't been done yet
      quality_measures.evaluate(x_axis, rulesset_name, dataset_name)
      fig_title = "Comparaison des distributions de deux indices"
    else:
      raise ValueError, 'Invalid x_axis: %s'%str(x_axis)
  else:
    x_axis_label = x_axis_info[x_axis][1]
    fig_title='Distribution des valeurs de %s en fonction de: %s'% (y_axis, x_axis_info[x_axis][0]) 


  # evaluate, if it hasn't been done yet
  quality_measures.evaluate(y_axis, rulesset_name, dataset_name)

  cur=conn.cursor()

  ds_id=dataset._dataset_id(dataset_name)
  rs_id=rules_set._id(rulesset_name)

  SQL_x_values = { 'FK_DATASET':   ds_id,
                   'FK_RULES_SET': rs_id }
  if not x_axis_isa_measure:
    SQL_x = "SELECT %(x_axis)s FROM EVALUATIONS t0, RULE t1 "\
            "WHERE t0.FK_DATASET=%(FK_DATASET)li "\
            "  AND t0.FK_RULE=t1.id AND t1.FK_RULES_SET=%(FK_RULES_SET)li "\
            "ORDER BY t1.id asc"
    SQL_x_values['x_axis'] = x_axis
  else:
    SQL_x = "SELECT t0.value FROM QUALITY_MEASURES t0, RULE t1 "\
            "WHERE t0.FK_DATASET=%(FK_DATASET)li AND name='%(measure)s'"\
            "  AND t0.FK_RULE=t1.id AND t1.FK_RULES_SET=%(FK_RULES_SET)li "\
            "  AND ifnull(t0.value, '+inf')!='+inf' " \
            "ORDER BY t1.id asc"
    SQL_x_values['measure'] = x_axis

  SQL_y = "SELECT t0.value FROM QUALITY_MEASURES t0, RULE t1 "\
          "WHERE t0.FK_DATASET=%(FK_DATASET)li AND name='%(measure)s'"\
          "  AND t0.FK_RULE=t1.id AND t1.FK_RULES_SET=%(FK_RULES_SET)li "\
          "  AND ifnull(t0.value, '+inf')!='+inf' " \
          "ORDER BY t1.id asc"

  cur.execute( SQL_x % SQL_x_values )

  x = [value[0] for value in cur.fetchall()]

  cur.execute( SQL_y %  { 'measure':      y_axis,
                          'FK_DATASET':   ds_id,
                          'FK_RULES_SET': rs_id })
  y = [value[0] for value in cur.fetchall()] # measure(rule)

  # do not 'import *': it overrides variable x
  from matplotlib.pylab import figure,rc
  if not fig:
    fig=figure(figure_num)
    fig=fig.add_subplot(111)
  figure_num += 1

  rc('lines', linewidth=0, color='b')
  if logx:
    fig.set_xscale('log')
  if logy:
    fig.set_yscale('log')

  #cf. lib/python2.3/site-packages/matplotlib/axes.py scatter()
  # -> CHANGED: padx, pady = 0,0 #0.05*w, 0.05*h
  # 's' : square
  # 'o' : circle
  # '^' : triangle up
  # '>' : triangle right
  # 'v' : triangle down
  # '<' : triangle left
  # 'd' : diamond
  # 'p' : pentagram
  # 'h' : hexagon
  # '8' : octagon
  size=16
  if plot_style == ',': # simulate 'pixel'
    plot_style = 's'
    size = 1

  fig.scatter(x,y,size,'w',marker=plot_style)

  fig.xaxis.get_label().set_text(x_axis_label)
  fig.xaxis.get_label().set_fontsize(20)
  #fig.xaxis.axisbg='#555555'

  fig.yaxis.get_label().set_text(r'%s'%y_axis)
  fig.yaxis.get_label().set_fontsize(20)
  fig.yaxis.get_label().set_verticalalignment('center')
  fig.yaxis.get_label().set_horizontalalignment('right')
  fig.yaxis.get_label().set_rotation('vertical')

  #title('uDistribution des valeurs de %s en fonction de: %s'% (measure, x_axis_info[x_axis][0]) )
  fig.set_title(fig_title)
  fig.grid(show_grid)
  
  if type(filename) == type(''):
    savefig(filename) #fig.savefig(filename)
  elif filename:
    for f in filename:
      savefig(f) #fig.savefig(f)
