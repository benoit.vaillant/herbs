# -*- coding: iso-8859-1 -*-
## LICENSE
"""
This modules defines the command 'import' used to load c4.5 or apriori files
into HERBS
"""
import getopt
from herbs.utils import log
from herbs.app import dataset

class cmd_import:
  """

  Implements command ``import``.
  
  """
  filename = None
  name = None  # defaults to filename
  format = None    # either 'c45' or 'apriori', cf. parse()
  c45_attr = ()
  csv_sep = ','
  csv_attrs = None
  csv_attrs_types = None
  
  def parse(self,args):
    """

    :except ValueError: if ``len(args)<2`` (the command needs a file path), or
      if there are unrecognized options.

    """
    log.debug("%s"%args)
    if not args[1:]:
      raise ValueError, "command import requires a file path"
    #import pdb ; pdb.set_trace()
    try:
      from getopt import getopt
      options,args = getopt(args[1:], 'hact:n:xs:l:t:',
                            ["help", "apriori",
                             "c45", "c45-tail-attr-name=", "name=",
                             "csv", "csv-separator=",
                             "csv-list-attr-names=", "csv-list-attr-types="])
    except:
      self.usage()
      raise ValueError, "Unable to parse options. Aborting"

    apriori = c45 = csv = name = False
    c45_attr = ()
    for k, v in options:
      print k,v
      if k in ('-h', '--help'): self.usage(); continue
      if k in ('-a', '--apriori'): apriori = True; continue

      if k in ('-c', '--c45'): c45 = True; continue
      if k in ('-t', '--c45-tail-attr-name'): c45_attr = v;
      if k in ('-n', '--name'): name = v;

      if k in ('-x', '--csv'): csv = True; continue
      if k in ('-s', '--csv-separator'): self.csv_sep = v; continue
      if k in ('-l', '--csv-list-attr-names'): self.csv_attrs = v; continue
      if k in ('-t', '--csv-list-attr-types'): self.csv_attrs_types=v; continue

    if len(args) < 1:
      self.usage()
      raise ValueError, "command import requires a file path"
    self.filename = args[0]

    import os
    self.name = name or os.path.splitext(os.path.basename(self.filename))[0]
    self.name = self.name.replace('.', '_') # TBD FIX THIS
    
    # Handling options related to c45 & apriori
    f = lambda a,b,c: a and not b and not c
    if not (f(apriori,c45,csv) or f(c45,apriori,csv) or f(csv,apriori,c45)):
      self.usage()
      raise ValueError, "Incorrect configuration of options"
    
    # Handling options related to dataset/csv import
    if len(self.csv_sep) != 1:
      raise ValueError, "csv separator must be a single character"

    if self.csv_attrs:
      self.csv_attrs = self.csv_attrs.split(':')
    if self.csv_attrs_types:
      self.csv_attrs_types = self.csv_attrs_types.split(':')
    if self.csv_attrs and self.csv_attrs_types:
      if len(self.csv_attrs) != len(self.csv_attrs_types):
        raise ValueError, 'Supplied attributes names and types must have the same length!'
      
    # TBD: check that the specific options are supplied iff the corresponding
    # TBD: command is set

    self.format = apriori and 'apriori' or c45 and 'c45' or 'csv'

    # if this is not c45, then it should be a false value so that
    # discard_no_head gets its default value
    self.c45_attr = c45 and (c45_attr,) or ()
    return args[1:]
    
  def run(self):
    """
    """
    if self.format in ('apriori', 'c45'):
      from herbs.app import rules_set
      rules_set.generic_rules_import(self.filename, self.format, self.name,
                                        *self.c45_attr)
    elif self.format == 'csv':
      dataset.import_csv(self.filename, self.name, self.csv_sep,
                         self.csv_attrs, self.csv_attrs_types)

    else:
      raise RuntimeError, "Unknown format %s"%self.format

  def usage(self):
    usage()


def usage():
  """
import [format] [options] <filename>  Parse a file and import its rules

Format
------
  Note: commands --c45 and --csv have specific options, detailed below

  The import command requires that one and only one format is given
  
  Importing a rules set:
      -a --apriori   rules in file are stored in the "apriori" format
      -c --c45       rules in file are stored in the "c4.5" format

      -n --name      the name of the rules set. If it is not supplied, it
                     defaults to the filename (without the extension)
  
  Importing a dataset
      -x --csv       data in file are stored in csv format 

      -n --name      the name of the dataset. If it is not supplied, it
                     defaults to the filename (without the extension)
      
Options
-------
  -h --help    this help

--c45 specific options
  -t --c45-tail-attr-name  description TBD

--csv specific options
  -s --csv-separator         the separator used in the csv. Defaults to ','

  -l --csv-list-attr-names   The list of the attributes' names, separated by
                             semi-colons, such as in: "attr1:attr2:attr3"

                             If it is not supplied, the attributes' names are
                             extracted from the first line of the file.

  -t --csv-list-attr-types   The list of the attributes' names, separated by
                             semi-colons.
                             Acceptable values are: 'float', 'int', 'text'
                             Example: "float:float:text"

                             If it is not supplied, then the attributes' types
                             will be guessed from the file contents. Warning:
                             this might take some time on big files.

  """
  return usage.__doc__
