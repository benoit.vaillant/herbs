#! /usr/bin/env python
# -*- coding: iso-8859-1 -*-
## LICENSE

from herbs.utils import log, FileFormatError, sql_normalize, openext
from herbs.app import conn

import csv
class my_excel_dialect(csv.excel):
  delimiter = ","  # this is a readonly attribute since py2.4 in csv.excel

# types are constants
TEXT='text'
INTEGER='integer'
FLOAT='float'
types=(TEXT,FLOAT,INTEGER)

def dominant_type(type1,type2):
  if type1==TEXT or type2==TEXT: return TEXT
  if type1==FLOAT or type2==FLOAT: return FLOAT
  return INTEGER

def guess_attributes_types(csvfile, separator, ignore_header):
  """
  Examines the lines in the supplied type and determines the types

  :Parameters:
    - `csvfile`: a file opened for reading. It can be any file-like object
      given that methods readline() is implemented

    - `separator`:
    - `ignore_header`:
    
  :return: a list of types

  :raise herbs.utils.FileFormatError:
  """
  line_nb=1
  
  dialect = my_excel_dialect()
  dialect.delimiter = separator
  csvreader = csv.reader(csvfile)

  if ignore_header:
    csvreader.next()

  line = csvreader.next()
  attrs_types = [INTEGER] * len(line)
  try:
    while True:
      types = [guess_attribute_type(v) for v in line]
    
      if len(types) != len(attrs_types):
        raise FileFormatError, "Error line %li: expected %li fields, got %li"%(line_nb, len(attrs_types), len(types) )
      
      attrs_types = [ dominant_type(t1, t2)
                      for t1,t2 in map(None, attrs_types, types)]
      #log.trace('attr_types: %s', attrs_types)
      # if at this point we only have TEXT, it's no use reading more
      if INTEGER not in attrs_types and FLOAT not in attrs_types:
        break;
    
      line = csvreader.next()
      line_nb += 1
  except StopIteration:
    pass
  log.info('Guessed attributes types for file %s: %s',
           getattr(csvfile,'name','<unknown>'), attrs_types)
  return attrs_types

def guess_attribute_type(value):
  """
  """
  value.strip()
  try:
    int(value)
    return INTEGER
  except ValueError:
    pass
  try:
    float(value)
    return FLOAT
  except ValueError:
    pass
  
  return TEXT


def import_csv(filename, dataset_name, separator,
               attrs_names=None, attrs_types=None):
  """

  :Parameters:
    - `filename`:
    - `dataset_name`:
    - `separator`:
    - `attrs_names`:
    - `attrs_types`:

  :return: the number of rows that were successfully imported
  
  :raise ValueError: if `filename` does not exist
  """
  nb_rows = 0

  import os
  if not os.path.isfile(filename):
    raise ValueError, 'Cannot find file %s'%filename

  file = openext(filename)

  import csv
  dialect = my_excel_dialect()
  dialect.delimiter = separator

  csvfile = csv.reader(file)

  ignore_header = False # used in the call to guess_attributes_types(), below
  if attrs_names is None:
    # the first line contains the header
    ignore_header = True
    attrs_names = csvfile.next()

  if not attrs_types:
    attrs_types = guess_attributes_types( openext(filename),
                                          separator, ignore_header )

  create_dataset(dataset_name, "TBD", attrs_names, attrs_types)
  try:
    for attrs_values in csvfile:
      # TBD: escape each values (esp. in TEXT fields!)
      attrs_values = tuple(attrs_values)

      conn.execute("INSERT INTO DS_%s VALUES "%dataset_name+str(attrs_values))
      nb_rows += 1

  except:
    conn.rollback()
    delete_dataset(dataset_name)
    raise
    
  conn.commit()
  return nb_rows

def create_dataset(name, comments, attrs_names, attrs_types):
  """

  :Parameters:
    - `name`:
    - `comments`:
    - `attrs_names`:
    - `attrs_types`:

  :except ValueError: if `name` is invalid, or if `attrs_names` and
    `attrs_types` do not have the same length.
  
  """
  # TBD: maker sure that name + attr_names are acceptable SQL identifiers
  if len(attrs_names) != len(attrs_types):
    raise ValueError, 'Parameters attrs_names and attrs_types should have the same length'
  if exists(name):
    raise ValueError, "A dataset with name %s already exists"%name

  columns_names = [ sql_normalize(attr_name) for attr_name in attrs_names ]
 
  attrs_names_types = map(None, attrs_names, attrs_types)
  try:
    conn.execute("INSERT INTO DATASET (id, name, comments, internals) "\
                 "VALUES (NULL, '%s', '%s', '%s')" % (name, comments,
                                                      str(attrs_names_types).replace("'","''")))
  except:
    conn.rollback()
    raise
  
  # Time to create table DS_name
  try:
    #name=sql_quote(name)
    create_statement = "CREATE TABLE 'DS_%s' ( "%name
    for attr_name, attr_type in map(None, columns_names, attrs_types):
      create_statement+='%s %s,'%(attr_name,attr_type.upper())
    create_statement=create_statement[:-1]+')'
    log.trace("creating dataset %s with statement: %s",name,create_statement)
    conn.execute(create_statement)
    conn.commit()
  except:
    conn.rollback()
    raise

def delete_dataset(name):
  """
  Removes a dataset from the HERBS' database

  :param name: name of the dataset to delete
  :except ValueError: if no dataset exists with such a `name`
  """
  cur=conn.cursor()
  cur.execute("DELETE FROM DATASET WHERE name='%s'"%name)
  cur.execute("DROP TABLE 'DS_%s'"%name)
  conn.commit()

def datasets():
  """

  Returns the set of available datasets, as a dictionary where keys are the
  datasets' names and the values are dictionaries mapping ``id`` and
  ``comments`` to their corresponding values.

  Example: ``{ 'cars': {'id':1, comments:'How people choose their car'} }``

  """
  # field 'internals' is deliberately ignored here
  cur = conn.cursor()
  cur.execute('SELECT id, name, comments FROM DATASET')
  res = cur.fetchall()
  d = {}
  [ d.setdefault(name, { 'id':id, 'comments':comments } )
    for id,name,comments in res ]
  return d

def exists(name, check_dataset_table=False):
  """
  Tells whether a dataset named `name` is available

  :Parameters:
    - `name`: name of a dataset

    - `check_dataset_table`: defaults to False. If set, then the method also
      checks that the table dedicated to the storing of the dataset's items is
      created; if that table does not exist, ``RuntimeError`` is raised. You
      normally do not need to set this, it is essentially present for testing
      purposes; methods in this module take care to create and delete the
      dedicated tables when needed.

  :rtype: boolean

  :raise RuntimeError: see parameter `check_dataset_table`'s description

  """
  cur=conn.cursor()
  cur.execute("SELECT COUNT(*) FROM DATASET WHERE name='%s'"%name)
  ok=cur.fetchone()[0]!=0
  if check_dataset_table:
    # also check that there exists a table named 'DS_%s'%name
    try:
      cur.execute("SELECT COUNT(*) FROM 'DS_%s'"%name)
      if not ok:
        # it is not registered within table DATASET but the SELECT on the
        # dedicated table did not raise: this shouldn't happen, that table
        # should have been dropped!
        raise RuntimeError, "Dataset %s is not registered as a dataset but its dedicated table exists"%name
      
    except: # sqlite.DatabaseError
      if ok:
        raise RuntimeError, "Dataset %s is registered as a dataset but its dedicated table does not exist"%name
  return ok

def dataset(name, limit=None, offset=0):
  """
  Returns the set of rows of the requested dataset
  
  :Parameters:
  - `name`: name of the dataset
  - `limit`, `offset`: TODO: they are ignored for now

  :Returns: the list of the rows in the dataset. Each row is itself an iterable
  object, and each one gives the columns' values in the same order, that is to
  say in the order returned by `attributes_names()`.

  :exception ValueError: if no such dataset exists

  :see: `nb_instances`
  """
  if not exists(name):
    raise ValueError, "Dataset %s does not exist"%name

  cur=conn.cursor()
  attrs=', '.join(attributes_names(name))
  cur.execute("SELECT %s from DS_%s"%(attrs,name))
  return cur.fetchall()
  
def _dataset_id(name):
  """
  Returns the id for the dataset named `name`

  :see: `_dataset_name_for_id`
  :except ValueError: if no such dataset exists
  """
  if name is None:
    raise ValueError, "None is an incorrect dataset's name"
  cur=conn.cursor()
  cur.execute("SELECT id FROM DATASET WHERE NAME='%s'"%name)
  res=cur.fetchall()
  if not res:
    raise ValueError, "Dataset %s does not exist"%name
  assert len(res)==1
  return res[0][0]


def _dataset_name_for_id(id):
  """
  Returns the name of the corresponding dataset

  :see: `_dataset_id`
  """
  cur=conn.cursor()
  cur.execute("SELECT name FROM DATASET WHERE id=%li"%id)
  res=cur.fetchall()
  if not res:
    raise ValueError, "Dataset w/ id:%li does not exist"%id
  assert len(res)==1
  return res[0][0]


def attributes(dataset_name):
  """
  Returns the list of attributes' names and types for the requested dataset.
  The items in the list are guaranteed to be given in the same order every
  time the method is called on a given dataset --and that order is the same
  for `attributes` and `attributes_names`.

  :param dataset_name: name of the dataset
  :return: a python list of ('attr_name', 'attr_type')
  :exception ValueError: if no such dataset exists
  :see: exists
  """
  cur=conn.cursor()
  cur.execute("SELECT internals FROM DATASET WHERE NAME='%s'"%dataset_name)
  res=cur.fetchone()
  if not res:
    raise ValueError, "Dataset %s does not exist"%dataset_name
  return eval(res[0])
  
def attributes_names(dataset_name):
  """
  Returns the list of attributes' names for the requested dataset.
  The items in the list are guaranteed to be given in the same order every
  time the method is called on a given dataset --and that order is the same
  for `attributes` and `attributes_names`.

  :param dataset_name: name of the dataset
  :return: a python list of string
  :exception ValueError: if no such dataset exists
  :see: attributes
  """
  return [attr[0] for attr in attributes(dataset_name)]
  
def nb_instances(name):
  """
  Returns the number of instances stored in a dataset

  :param name: name of the dataset
  :exception ValueError: if no such dataset exists
  """
  if not exists(name):
    raise ValueError, "Dataset %s does not exist"%name
  cur=conn.cursor()
  cur.execute("SELECT COUNT(*) FROM 'DS_%s'"%name)
  return cur.fetchone()[0]

def evaluate_objectSet_with_rule(name, rule): # TBD
  """

  Evaluates a set of objects wrt a given rule, and returns the three computed
  properties: (na, nb and nab), where:

  - na is the number of objects fulfilling the rule's head,

  - nb the number of objects fulfulling the rule's tail,

  - nab the number of objects matching the whole rule

  """
  cur=conn.cursor()
  _evaluate_objectSet_with_rule(name, rule, cur)

def _evaluate_objectSet_with_rule(name, rule, cur):
  select='select count(*) from bdc_%s where '%name

  head=rule.head
  tail=rule.tail
  
  h_n_t=select+head+" and "+tail
  head=select+head
  tail=select+tail
  
  #print head, tail, h_n_t
  
  cur.execute(head)
  na=cur.fetchone()[0]
  cur.execute(tail)
  nb=cur.fetchone()[0]
  cur.execute(h_n_t)
  nab=cur.fetchone()[0]
  
  return na, nb, nab
