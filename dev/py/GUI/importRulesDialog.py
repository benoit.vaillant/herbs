#Boa:Dialog:importRulesDialog

from wxPython.wx import *
from wxPython.lib.buttons import *

import os

def create(parent):
    return importRulesDialog(parent)

[wxID_IMPORTRULESDIALOG, wxID_IMPORTRULESDIALOGAPRIORIRADIO, 
 wxID_IMPORTRULESDIALOGBROWSEBTN, wxID_IMPORTRULESDIALOGC45RADIO, 
 wxID_IMPORTRULESDIALOGC45TAILATTRNAME, 
 wxID_IMPORTRULESDIALOGC45TAILATTRNAMELABEL, 
 wxID_IMPORTRULESDIALOGCANCELBUTTON, wxID_IMPORTRULESDIALOGFILENAMEFIELD, 
 wxID_IMPORTRULESDIALOGFULLDECISIONTREECB, 
 wxID_IMPORTRULESDIALOGIMPORTFILELABEL, wxID_IMPORTRULESDIALOGOKBUTTON, 
 wxID_IMPORTRULESDIALOGPANEL1, wxID_IMPORTRULESDIALOGRSNAMECHECKBOX, 
 wxID_IMPORTRULESDIALOGRULESSETNAME, 
 wxID_IMPORTRULESDIALOGSIMPLIFIEDDECISIONTREECB, 
] = map(lambda _init_ctrls: wxNewId(), range(15))

class importRulesDialog(wxDialog):

  def _init_ctrls(self, prnt):
    # generated method, don't edit
    wxDialog.__init__(self, id=wxID_IMPORTRULESDIALOG, name='importRulesDialog',
          parent=prnt, pos=wxPoint(308, 420), size=wxSize(329, 315),
          style=wxDEFAULT_DIALOG_STYLE, title='Import a new set of rules')
    self.SetClientSize(wxSize(329, 315))

    self.panel1 = wxPanel(id=wxID_IMPORTRULESDIALOGPANEL1, name='panel1',
          parent=self, pos=wxPoint(8, 24), size=wxSize(312, 112),
          style=wxRAISED_BORDER | wxTAB_TRAVERSAL)
    self.panel1.Show(True)

    self.c45Radio = wxRadioButton(id=wxID_IMPORTRULESDIALOGC45RADIO,
          label='C4.5', name=u'c45Radio', parent=self.panel1, pos=wxPoint(24,
          8), size=wxSize(64, 24), style=wxRB_GROUP)
    self.c45Radio.SetValue(True)
    EVT_RADIOBUTTON(self.c45Radio, wxID_IMPORTRULESDIALOGC45RADIO,
          self.On_RulesTypeRBSelection)

    self.c45TailAttrNameLabel = wxStaticText(id=wxID_IMPORTRULESDIALOGC45TAILATTRNAMELABEL,
          label=u'Tail Attribute Name:', name=u'c45TailAttrNameLabel',
          parent=self.panel1, pos=wxPoint(48, 40), size=wxSize(104, 18),
          style=0)

    self.c45TailAttrName = wxTextCtrl(id=wxID_IMPORTRULESDIALOGC45TAILATTRNAME,
          name=u'c45TailAttrName', parent=self.panel1, pos=wxPoint(152, 36),
          size=wxSize(152, 22), style=0, value=u'')
    EVT_TEXT(self.c45TailAttrName, wxID_IMPORTRULESDIALOGC45TAILATTRNAME,
          self.updateOkButtonState)

    self.aprioriRadio = wxRadioButton(id=wxID_IMPORTRULESDIALOGAPRIORIRADIO,
          label='apriori', name=u'aprioriRadio', parent=self.panel1,
          pos=wxPoint(24, 64), size=wxSize(64, 24), style=0)
    self.aprioriRadio.SetValue(False)
    EVT_RADIOBUTTON(self.aprioriRadio, wxID_IMPORTRULESDIALOGAPRIORIRADIO,
          self.On_RulesTypeRBSelection)

    self.RSNameCheckbox = wxCheckBox(id=wxID_IMPORTRULESDIALOGRSNAMECHECKBOX,
          label=u'Rules Set Name:', name=u'RSNameCheckbox', parent=self,
          pos=wxPoint(16, 160), size=wxSize(112, 24), style=0)
    self.RSNameCheckbox.SetValue(False)
    EVT_CHECKBOX(self.RSNameCheckbox, wxID_IMPORTRULESDIALOGRSNAMECHECKBOX,
          self.OnRSNameCheckbox)

    self.rulesSetName = wxTextCtrl(id=wxID_IMPORTRULESDIALOGRULESSETNAME,
          name='rulesSetName', parent=self, pos=wxPoint(136, 160),
          size=wxSize(184, 22), style=0, value='')
    self.rulesSetName.Enable(False)
    EVT_TEXT(self.rulesSetName, wxID_IMPORTRULESDIALOGRULESSETNAME,
          self.On_RulesSetNameText)

    self.filenameField = wxTextCtrl(id=wxID_IMPORTRULESDIALOGFILENAMEFIELD,
          name='filename', parent=self, pos=wxPoint(8, 210), size=wxSize(312,
          22), style=0, value='')
    EVT_TEXT(self.filenameField, wxID_IMPORTRULESDIALOGFILENAMEFIELD,
          self.On_FilenameFieldText)

    self.ImportFileLabel = wxStaticText(id=wxID_IMPORTRULESDIALOGIMPORTFILELABEL,
          label='Import file:', name='ImportFileLabel', parent=self,
          pos=wxPoint(8, 194), size=wxSize(56, 16), style=0)

    self.BrowseBTN = wxGenButton(id=wxID_IMPORTRULESDIALOGBROWSEBTN,
          label='Browse...', name='BrowseBTN', parent=self, pos=wxPoint(232,
          232), size=wxSize(81, 27), style=0)
    EVT_BUTTON(self.BrowseBTN, wxID_IMPORTRULESDIALOGBROWSEBTN,
          self.On_BrowseButton)

    self.OkButton = wxGenButton(id=wxID_IMPORTRULESDIALOGOKBUTTON, label='Ok',
          name='OkButton', parent=self, pos=wxPoint(16, 272), size=wxSize(81,
          27), style=0)
    self.OkButton.SetToolTipString('Import the selected file into the application')
    self.OkButton.SetForegroundColour(wxColour(0, 0, 0))
    self.OkButton.Enable(False)
    self.OkButton.SetFont(wxFont(12, wxSWISS, wxNORMAL, wxNORMAL, False,''))
    EVT_BUTTON(self.OkButton, wxID_IMPORTRULESDIALOGOKBUTTON, self.on_OkBTN)

    self.CancelButton = wxGenButton(id=wxID_IMPORTRULESDIALOGCANCELBUTTON,
          label='Cancel', name='CancelButton', parent=self, pos=wxPoint(128,
          272), size=wxSize(81, 27), style=0)
    self.CancelButton.SetBezelWidth(2)
    self.CancelButton.SetForegroundColour(wxColour(0, 0, 0))
    EVT_BUTTON(self.CancelButton, wxID_IMPORTRULESDIALOGCANCELBUTTON,
          self.on_CancelBTN)

    self.FullDecisionTreeCB = wxRadioButton(id=wxID_IMPORTRULESDIALOGFULLDECISIONTREECB,
          label='Full Decision Tree', name='FullDecisionTreeCB',
          parent=self.panel1, pos=wxPoint(96, 48), size=wxSize(152, 24),
          style=wxRB_GROUP)
    self.FullDecisionTreeCB.SetValue(True)
    self.FullDecisionTreeCB.Enable(False)
    self.FullDecisionTreeCB.Show(False)
    EVT_RADIOBUTTON(self.FullDecisionTreeCB,
          wxID_IMPORTRULESDIALOGFULLDECISIONTREECB,
          self.On_RulesTypeRBSelection)

    self.SimplifiedDecisionTreeCB = wxRadioButton(id=wxID_IMPORTRULESDIALOGSIMPLIFIEDDECISIONTREECB,
          label='Simplified Decision Tree', name='SimplifiedDecisionTreeCB',
          parent=self.panel1, pos=wxPoint(96, 72), size=wxSize(152, 24),
          style=0)
    self.SimplifiedDecisionTreeCB.SetValue(False)
    self.SimplifiedDecisionTreeCB.Enable(False)
    self.SimplifiedDecisionTreeCB.Show(False)

  def __init__(self, parent):
        self._init_ctrls(parent)

  def On_RulesTypeRBSelection(self, event):
    if event.GetId()==wxID_IMPORTRULESDIALOGAPRIORIRADIO:
          self.FullDecisionTreeCB.Enable(True)
          self.SimplifiedDecisionTreeCB.Enable(True)
          self.c45TailAttrNameLabel.Enable(False)
          self.c45TailAttrName.Enable(False)

    if event.GetId()==wxID_IMPORTRULESDIALOGC45RADIO:
          self.FullDecisionTreeCB.Enable(False)
          self.SimplifiedDecisionTreeCB.Enable(False)
          self.c45TailAttrNameLabel.Enable(True)
          self.c45TailAttrName.Enable(True)

    self.updateOkButtonState()
    event.Skip()


  def On_BrowseButton(self, event):
    dlg = wxFileDialog(self, "Choose a file", os.getcwd(), "",
                       "All files (*.*)|*.*",
                       wxOPEN | wxCHANGE_DIR
                       )
    if dlg.ShowModal() == wxID_OK:
        self.filenameField.SetValue(dlg.GetPath())
    dlg.Destroy()
    event.Skip()

  def on_OkBTN(self, event):
    self.EndModal(wxID_OK)

  def on_CancelBTN(self, event):
    self.EndModal(wxID_CANCEL)

  def updateOkButtonState(self, event=None):
    """
    Enables the 'Ok' button iff:

      - the rules set has a name,
      - it has a valid filename
      - the tail-attribute name is specified when the format is c45

    :param event: ignored
    """
    import os
    ok = self.rulesSetName.GetValue()
    ok = ok and os.path.isfile(self.filenameField.GetValue())
    ok = ok and ((self.c45Radio.GetValue() and self.c45TailAttrName.GetValue())
                 or self.aprioriRadio.GetValue())
    self.OkButton.Enable(bool(ok))
        
  def On_FilenameFieldText(self, event):
    ok = 1
    if not self.rulesSetName.GetValue() or not self.RSNameCheckbox.IsChecked():
      self.rulesSetName.SetValue(os.path.splitext(os.path.basename(self.filename()))[0])

    self.updateOkButtonState()
    event.Skip()

  def On_RulesSetNameText(self, event):
    self.updateOkButtonState()
    event.Skip()

  def OnRSNameCheckbox(self, event):
    self.rulesSetName.Enable(self.RSNameCheckbox.IsChecked())
    if not self.RSNameCheckbox.IsChecked() and self.filename():
      self.rulesSetName.SetValue(os.path.splitext(os.path.basename(self.filename()))[0])


    event.Skip()

  def name(self):
    return self.rulesSetName.GetValue()

  def filename(self):
    return self.filenameField.GetValue()

  def format(self):
    return self.c45Radio.GetValue() and 'c45' or 'apriori'

  def c45_tailAttrName(self):
    return self.c45TailAttrName.GetValue()
