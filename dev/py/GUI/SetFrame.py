#Boa:Frame:SetFrame

from wxPython.wx import *
from wxPython.grid import *

def create(parent, ds_name=None, rs_name=None):
    return SetFrame(parent, ds_name, rs_name)

[wxID_WXSETFRAME, wxID_SETGRID, wxID_STATUSBAR,
] = map(lambda _init_ctrls: wxNewId(), range(3))

DEFAULT_W, DEFAULT_H = 800, 600
class SetFrame(wxFrame):
  
  def _init_ctrls(self, prnt):
    wxFrame.__init__(self, id=wxID_WXSETFRAME, name='', parent=prnt,
                     #pos=wxPoint(183, 530),
                     size=wxSize(DEFAULT_W, DEFAULT_H),
                     style=wxDEFAULT_FRAME_STYLE, title=u'')

    self.SetClientSize(wxSize(DEFAULT_W, DEFAULT_H))

    self.statusBar = wxStatusBar(id=wxID_STATUSBAR, name='status bar',
                                 parent=self, style=0)
    self.SetStatusBar(self.statusBar)

    self.grid = wxGrid(id=wxID_SETGRID, name='grid',
          parent=self, #pos=wxPoint(0, 0), size=wxSize(796, 427),
          style=wxHSCROLL|wxVSCROLL)
    
    self.grid.SetColLabelSize(20)
    self.grid.SetRowLabelSize(0)
    self.grid.EnableEditing(False)
    self.grid.EnableGridLines(True)
    self.grid.DisableDragRowSize()
    
    self.grid.AdjustScrollbars()
    EVT_GRID_COL_SIZE(self.grid, self.isResized)


  def __init__(self, parent, ds_name=None, rs_name=None):
    assert not (ds_name and rs_name), \
           "At most one of the parameters ds_name/rs_name must be given"

    self._init_ctrls(parent)
    if ds_name:
        self.init_with_ds(ds_name)
    if rs_name:
        self.init_with_rs(rs_name)

  def isResized(self, event):
    ""
    self.grid.AdjustScrollbars()
  
  def init_columns(self, columns):
    """
    This is the first step to create the grid, consisting in giving the
    columns names.

    :param columns: list of columns' names (strings)
    """
    self.grid.CreateGrid(0, len(columns))
      
    for idx, col in enumerate(columns):
      self.grid.SetColLabelValue(idx, col)
  
    
  def init_with_ds(self, name):
    """
    Initializes the frame's grid to display the dataset's rows in the
    frame's grid
    :param name: a dataset's name
    """
    from herbs.app import dataset as ds

    self.SetTitle("Dataset: "+name)
    self.statusBar.SetStatusText("Dataset: "+name)
    grid = self.grid

    grid.BeginBatch()
    wxBeginBusyCursor()
    wxSafeYield()
    try:
      self.init_columns(["#"] + ds.attributes_names(name))
  
  
      grid.AppendRows(ds.nb_instances(name))
      for row, data in enumerate(ds.dataset(name)): # TODO: memory!!
        # if not row%100: wxSafeYield()
        grid.SetCellValue(row, 0, str(row+1))
        for col, d in enumerate(data):
          grid.SetCellValue(row, col+1, str(d))

      #grid.AutoSizeColumns()
      
    finally:
      wxEndBusyCursor()
      grid.EndBatch()
    

  def init_with_rs(self, name):
    """
    Initializes the frame's grid to display the rules in the frame's grid
    :param name: a dataset's name
    """
    from herbs.app import rules_set as rs

    self.SetTitle("Rules set: "+name)
    self.statusBar.SetStatusText("Rules set: "+name)
    grid = self.grid

    grid.BeginBatch()
    wxBeginBusyCursor()
    wxSafeYield()
    try:
      self.init_columns(["id", "Head", "Tail"])
  
      grid.AppendRows(rs.nb_rules(name))
      for row, rule in enumerate(rs.rules_named(name)): # TODO: memory!!
        #if not row%100: wxSafeYield()
        grid.SetCellValue(row, 0, str(rule['id']))
        grid.SetCellValue(row, 1, rule['head'])
        grid.SetCellValue(row, 2, rule['tail'])

      #grid.AutoSizeColumns()
      w, h = self.GetClientSizeTuple()
      grid.SetColSize(0, w/10)
      grid.SetColSize(1, 6*w/10)
      grid.SetColSize(2, 3*w/10)

    finally:
      wxEndBusyCursor()
      grid.EndBatch()
    
  def init_with_compatible_couple(self, rs, ds):
    """
    Initializes the frame's grid to display the evaluations of the rules in
    `rs` with respect to `ds`

    :Parameters:
      - `rs`: a ruleset's name
      - `ds`: a dataset's name

    """
    from herbs.app import rules_set, dataset, evaluations
    rs_id = rules_set._id(rs)
    ds_id = dataset._dataset_id(ds)

    _title="Evaluation for rules set: %s wrt the dataset: %s"%(rs, ds)
    self.SetTitle(_title)
    self.statusBar.SetStatusText(_title)
    grid = self.grid

    grid.BeginBatch()
    wxBeginBusyCursor()
    wxSafeYield()
    try:
      self.init_columns(["id", "Head", "Tail", "na", "nb", "nab", "nab_"])
  
      grid.AppendRows(rules_set.nb_rules(rs))
      for row, rule in enumerate(rules_set.rules_named(rs)): # TODO: memory!!
        #if not row%100: wxSafeYield()
        grid.SetCellValue(row, 0, str(rule['id']))
        grid.SetCellValue(row, 1, rule['head'])
        grid.SetCellValue(row, 2, rule['tail'])
        evals = evaluations.evaluation(rule['id'], ds_id)
        grid.SetCellValue(row, 3, str(evals['na']))
        grid.SetCellValue(row, 4, str(evals['nb']))
        grid.SetCellValue(row, 5, str(evals['nab']))
        grid.SetCellValue(row, 6, str(evals['nab_']))

      #grid.AutoSizeColumns()
      w, h = self.GetClientSizeTuple()
      grid.SetColSize(0, 2*w/20)
      grid.SetColSize(1, 6*w/20)
      grid.SetColSize(2, 4*w/20)
      grid.SetColSize(3, 2*w/20)
      grid.SetColSize(4, 2*w/20)
      grid.SetColSize(5, 2*w/20)
      grid.SetColSize(6, 2*w/20)

    finally:
      wxEndBusyCursor()
      grid.EndBatch()
    
  def init_with_N_best_rules(self, rs, ds, measure, N, k=None):
    """
    Initializes the frame's grid to display:

    - if `k` is None, the `N` best rules in `rs` wrt their values evaluated by
      `measure` on the compatible couple (`rs`,`ds`)

    - or otherwise the rules that are ranked at least k times in the N best
      rules (with N and `measure` being a list of measures' names, see
      `herbs.app.compatible.N_best_rules_among_k`)

    :Parameters:
      - `rs`: a ruleset's name
      - `ds`: a dataset's name
      - `measure`: a measure's name or a list of measures' names, see
        description above
      - `k`: either None or an int. see description above
    
    """
    from herbs.app import compatible, dataset

    grid = self.grid

    self.init_right_click_on_rule( rule_id_column = 1,
                                   dataset_id = dataset._dataset_id(ds) )
    
    grid.BeginBatch()
    wxBeginBusyCursor()
    wxSafeYield()
    try:
      if k is None:
        self.init_columns(["#", "id", "Head", "Tail", "Value"])
      else:
        self.init_columns(["#", "id", "Head", "Tail", "k", "Selected by"])

      #NB: float('+inf') is invalid on windows platforms
      from numarray import ieeespecial as ieee
      old_value=ieee.plus_inf
      row_idx=rank=0
      placed_equal=1

      if k is None:
        func=lambda rs,ds,measure,N,k: compatible.N_best_rules(rs,ds,measure,N)
      else:
        func=compatible.N_best_rules_among_k
      for rule,value in func(rs,ds,measure,N,k):
        #if not row%100: wxSafeYield()
        if value<old_value:
          rank+=placed_equal
          placed_equal=1
        else:
          placed_equal+=1
        old_value = value
          
        grid.AppendRows(1)
        #row_idx = rank+placed_equal-2
        grid.SetCellValue(row_idx, 0, str(rank))
        grid.SetCellValue(row_idx, 1, str(rule['id']))
        grid.SetCellValue(row_idx, 2, rule['head'])
        grid.SetCellValue(row_idx, 3, rule['tail'])
        if k is None:
          grid.SetCellValue(row_idx, 4, str(value))
        else:
          grid.SetCellValue(row_idx, 4, str(rule.count))
          rule['selected_by'].sort()
          grid.SetCellValue(row_idx, 5, ', '.join(rule['selected_by']))
          
        row_idx+=1
        
      grid.AutoSizeColumns()

      if k is None:
        msg = "N best rules for %s:%s with measure:%s"%(rs,ds,measure)
        msg += " -- Requested: %li, displayed: %li"%(N,row_idx)
      else:
        msg = "Rules ranked k times in the N best for %s:%s"%(rs,ds)
        msg += " with measures:%s"%(measure,)
        msg += " -- N:%li k:%li"%(N,k)

      self.SetTitle(msg)
      self.statusBar.SetStatusText(msg)
      
    finally:
      wxEndBusyCursor()
      grid.EndBatch()

  def init_right_click_on_rule(self, rule_id_column, dataset_id):
    """
    Binds the right click event to `onRightClickOnRule`
    :param rule_id_column: the index of the column where the id of the rule
      can be found (1st column==index 0)
    """
    EVT_GRID_CELL_RIGHT_CLICK(self.grid, self.onRightClickOnRule)
    self.rule_id_column = rule_id_column
    self.dataset_id = dataset_id
      
  def onRightClickOnRule(self, event):
    """
    Triggered upon a right click on a rule; this method expects that
    `init_right_click_on_rule` has been called before.  Show a popup menu
    showing the value of ``na``,``nb``,``nab`` and ``nab_`` as returned by
    `herbs.app.evaluations.evaluation()`.
    """
    from herbs.app import evaluations
    rule_id = int( self.grid.GetCellValue(event.GetRow(),self.rule_id_column) )
    evals = evaluations.evaluation(rule_id, self.dataset_id)
    menu_items = [ "Evals. for rule #%li"%rule_id, ""]
    menu_items += [ "%s: %s"%(k, evals[k]) for k in 'na','nb','nab','nab_']
    from utilities import wxmenus_from_menus
    menu = wxmenus_from_menus(menu_items, lambda event: event.Skip(), {})
    self.PopupMenu( menu, event.GetPosition() )
    event.Skip()

