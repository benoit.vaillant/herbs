# -*- coding: iso-8859-1 -*-
## LICENSE
"""
Controller for the main frame

"""
from herbs.app import rules_set, dataset
from wxPython.wx import wxSafeYield

def openedProject():
  "Returns the file path of the current opened project, or None"
  from herbs.app import conn
  return conn.current_dbname

def isaProjectOpened():
  "Tell whether a project is currently opened"
  return openedProject() is not None

def disableGrids(frame):
  "Disable the frame's grids"
  enableGrids(frame, False)

def enableGrids(frame, bool=True):
  "Enable the frame's grids, or disables them if `bool` is ``False``"
  size = bool and 20 or 0
  frame.main_notebook.Enable(bool)
  frame.rulesGrid.Enable(bool)
  frame.rulesGrid.SetColLabelSize(size)
  frame.datasetsGrid.Enable(bool)
  frame.datasetsGrid.SetColLabelSize(size)
  frame.compatibilityGrid.Enable(bool)
  frame.compatibilityGrid.SetColLabelSize(size)

def openProject(frame, projectPath, create=False):
  frame.SetTitle("HERBS - "+projectPath)
  from herbs.app import conn
  try:
    conn.init(projectPath, create)
  except:
    return #TBD dialog

  onOpenProject(frame)

def onOpenProject(frame, bool=True):
  enableGrids(frame, bool)
  enableWholeMenu(frame.rulessets_menu, bool)
  enableWholeMenu(frame.datasets_menu, bool)
  from main_frame import wxID_FILE__CLOSE_PRJ # TBD correct this!!
  frame.file_menu.Enable(wxID_FILE__CLOSE_PRJ, bool)
  refreshGrids(frame, show_progress=True)

  frame.rulesGrid.AutoSizeColumns()
  frame.rulesGrid.AdjustScrollbars()
  frame.datasetsGrid.AutoSizeColumns()
  frame.datasetsGrid.AdjustScrollbars()
  frame.compatibilityGrid.AutoSizeColumns()
  frame.compatibilityGrid.AdjustScrollbars()
  
def onCloseProject(frame):
  frame.SetTitle("HERBS")
  onOpenProject(frame, False)  

def refreshGrids(frame, show_progress=False):
  page_selected=1
  if show_progress:
    page_selected=frame.main_notebook.SetSelection(0)
  fill_rulesGrid(frame)
  if show_progress:
    frame.main_notebook.SetSelection(1)
  fill_datasetsGrid(frame)
  if show_progress:
    frame.main_notebook.SetSelection(2)
    wxSafeYield()
  import main_compatibility_ctrl
  main_compatibility_ctrl.fill_compatibilityGrid(frame.compatibilityGrid, refresh=True)
  if show_progress:
    frame.main_notebook.SetSelection(page_selected)

##
## Rules set grid
def fill_rulesGrid(frame):
  grid=frame.rulesGrid
  n=grid.GetNumberRows()
  wxSafeYield()
  if openedProject():
    rs=rules_set.rules_sets() #id_names_desc
  else:
    rs={}
  wxSafeYield()
  nb_rules=len(rs)
  if nb_rules>n:
      grid.AppendRows(nb_rules-n)
  if nb_rules<n:
      grid.DeleteRows(pos=0, numRows=n-nb_rules)
  idx=0
  names=rs.keys()
  names.sort()
  for name in names:
      wxSafeYield()
      id=rs[name]['id']
      desc=rs[name]['comments']
      grid.SetRowLabelValue(idx, str(id))
      grid.SetCellValue(idx, 0, name)
      grid.SetCellValue(idx, 1, desc)
      idx+=1

def import_rs(frame, name, filename, format, c45_tailAttrName=None):
  from herbs.app import rules_set

  c45_attr = c45_tailAttrName and (c45_tailAttrName,) or ()
  if format in ('c45', 'apriori'):
    rules_set.generic_rules_import(filename, format, name,
                                   *c45_attr)
  else:
    raise ValueError, 'Unknown format %s'%format
  refreshGrids(frame)
  
def delete_rs(frame, rs_name):
  from herbs.app import rules_set
  rules_set.delete_rules_set(rs_name)
  refreshGrids(frame)

def rename_rs(frame, rs_name, rs_new_name):
  from herbs.app import rules_set
  rules_set.rename_rules_set(rs_name, rs_new_name)
  refreshGrids(frame)
  
##
## Dataset grid
def fill_datasetsGrid(frame):
  grid=frame.datasetsGrid
  n=grid.GetNumberRows()
  wxSafeYield()
  if openedProject():
    rs=dataset.datasets() #id_names_desc
  else:
    rs={}
  wxSafeYield()
  nb_rules=len(rs)
  if nb_rules>n:
      grid.AppendRows(nb_rules-n)
  if nb_rules<n:
      grid.DeleteRows(pos=0, numRows=n-nb_rules)
  idx=0
  names=rs.keys()
  names.sort()
  for name in names:
      wxSafeYield()
      id=rs[name]['id']
      desc=rs[name]['comments']
      grid.SetRowLabelValue(idx, str(id))
      grid.SetCellValue(idx, 0, name)
      grid.SetCellValue(idx, 1, desc)
      idx+=1

def import_ds(frame, name, filename, separator):
  from herbs.app import dataset
  dataset.import_csv(filename, name, separator)
  refreshGrids(frame)

def delete_ds(frame, ds_name):
  from herbs.app import dataset
  dataset.delete_dataset(ds_name)
  refreshGrids(frame)

#def rename_ds(frame, ds_name, ds_new_name):
#  from herbs.app import rules_set
#  dataset.rename_dataset(ds_name, ds_new_name)
#  refreshGrids(frame)

# utils
def enableWholeMenu(menu, bool=True):
  for menuItems in menu.GetMenuItems():
    menu.Enable(menuItems.GetId(), bool)

def disableWholeMenu(menu):
  enableWholeMenu(menu, False)
