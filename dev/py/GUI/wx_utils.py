# -*- coding: iso-8859-1 -*-
## LICENSE
import string, wx

ALPHA_ONLY=1
DIGIT_ONLY=2

class TextFieldValidator(wx.PyValidator):
  def __init__(self, flag=None, pyVar=None, bounds=None,
               transfer_from_window=None):
    wx.PyValidator.__init__(self)
    self.flag = flag
    self.bounds = bounds
    self.transfer_from = transfer_from_window
    self.Bind(wx.EVT_CHAR, self.OnChar)
    
  def Clone(self):
    return TextFieldValidator(self.flag, bounds=self.bounds,
                              transfer_from_window=self.transfer_from)
  
  def Validate(self, win):
    tc = self.GetWindow()
    val = tc.GetValue()
    if self.flag == ALPHA_ONLY:
        for x in val:
            if x not in string.letters:
                return False
    elif self.flag == DIGIT_ONLY:
        for x in val:
            if x not in string.digits:
                return False
        if self.bounds:
          if int(val)<self.bounds[0]:
            return False
          if int(val)>self.bounds[1]:
            tc.SetValue(str(self.bounds[1]))
            return False
    return True
  
  def OnChar(self, event):
    key = event.KeyCode()
    
    if key < wx.WXK_SPACE or key == wx.WXK_DELETE or key > 255:
        event.Skip()
        return
    
    if self.flag == ALPHA_ONLY and chr(key) in string.letters:
        event.Skip()
        return
    
    if self.flag == DIGIT_ONLY and chr(key) in string.digits:
        event.Skip()
        return
    
    if not wx.Validator_IsSilent():
        wx.Bell()
    
    # Returning without calling even.Skip eats the event before it
    # gets to the text control
    return
  
  def TransferToWindow(self):
    return True

  def TransferFromWindow(self):
    if self.transfer_from:
      self.transfer_from(self.GetWindow().GetValue())

    ## copy the value in the text field back to the parent dialog
    #self.GetWindow().GetParent().SetValue(self.GetWindow().GetValue())
    #return True


# [c for c in w.GetChildren() if isinstance(c, wxTextCtrl)]
# c.SetValidator(MyValidator(ALPHA_ONLY))
# w.SetValue('initial')
# w.ShowModal()
# w.GetValue()
